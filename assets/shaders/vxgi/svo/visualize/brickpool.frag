#version 450

#define CAMERA_BINDING 0

#include libs/camera.glsl

layout(location = 1) uniform uint level;

layout(std430, binding = 2) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

layout(rgba8, binding = 3) uniform coherent volatile image3D brickpool;

in flat ivec3 brick_address_geometry;

layout (location = 0) out vec4 color;

void main()
{
    vec4 value = imageLoad(brickpool, brick_address_geometry);

    // if (value.a == 0.0) {
    //     discard;
    // }

    color = vec4(1.0);
}