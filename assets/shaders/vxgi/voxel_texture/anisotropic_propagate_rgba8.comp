#version 450 core

#define VXGI_OPTIONS_BINDING 0

#define VOXEL_ALBEDO_BINDING 1
#define VOXEL_NORMAL_BINDING 2

#define VOXEL_RADIANCE_BINDING 3

#define VOXEL_RADIANCE_MIPS_BINDING 4
#define VOXEL_EMISSION_MIPS_BINDING 10

#include vxgi/options.glsl

layout (local_size_x = 8, local_size_y = 8, local_size_z = 8) in;

layout (binding = VOXEL_ALBEDO_BINDING)   uniform sampler3D voxel_albedo;
layout (binding = VOXEL_NORMAL_BINDING)   uniform sampler3D voxel_normal;

layout (binding = VOXEL_RADIANCE_BINDING, rgba8) uniform image3D voxel_radiance;

layout(binding = VOXEL_RADIANCE_MIPS_BINDING) uniform sampler3D volume_radiance_mip[6];
layout(binding = VOXEL_EMISSION_MIPS_BINDING) uniform sampler3D volume_emission_mip[6];


const float PI = 3.14159265f;
const float EPSILON = 1e-30;
const uint pow2[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};

// four cones
const vec3 propagationDirections[] =
{
    vec3(0.0f, 1.0f, 0.0f),
    vec3(0.0f, 0.5f, 0.866025f),
    vec3(0.754996f, 0.5f, -0.4330128f),
    vec3(-0.754996f, 0.5f, -0.4330128f)
};

const float diffuseConeWeights[] =
{
    PI / 3.0f,
    2.0f * PI / 9.0f,
    2.0f * PI / 9.0f,
    2.0f * PI / 9.0f,
};

vec4 vxgi_sample_function(vec3 position, vec3 weight, uvec3 face, float lod, VoxelTextureType type)
{
    face.x = clamp(face.x, 0U, 1U);
    face.y = clamp(face.y, 2U, 3U);
    face.z = clamp(face.z, 4U, 5U);

    float anisotropic_level = max(lod - 1.0f, 0.0f);
    vec4  anisotropic_sample = vec4(0.0f);
    if (type == VoxelTextureTypeRadiance) {
        anisotropic_sample = weight.x * textureLod(volume_radiance_mip[face.x], position, anisotropic_level)
                           + weight.y * textureLod(volume_radiance_mip[face.y], position, anisotropic_level)
                           + weight.z * textureLod(volume_radiance_mip[face.z], position, anisotropic_level);
    } 
    else if (type == VoxelTextureTypeEmission) {
        anisotropic_sample = weight.x * textureLod(volume_emission_mip[face.x], position, anisotropic_level)
                           + weight.y * textureLod(volume_emission_mip[face.y], position, anisotropic_level)
                           + weight.z * textureLod(volume_emission_mip[face.z], position, anisotropic_level);
    }

    return anisotropic_sample;
}

vec4 trace_cone(vec3 position, vec3 normal, vec3 direction, float aperture, bool trace_occlusion) {
  float max_distance = 1.0;

  // Convert position from world space to texture space
  position = position * 0.5 + 0.5;

  // which of the 3 faces of voxel can be seen from cone direction
  uvec3 visible_faces;
  visible_faces.x = (direction.x < 0.0) ? 0 : 1;
  visible_faces.y = (direction.y < 0.0) ? 2 : 3;
  visible_faces.z = (direction.z < 0.0) ? 4 : 5;
  // weight per axis for aniso sampling
  vec3 weight = direction * direction;

  float voxel_size = 1.0 / float(vxgi_options.dimension);

  // move further to avoid self collision
  float t = 2.0 * voxel_size;

  // final results
  vec4 cone_sample = vec4(0.0f);
  float occlusion = 0.0;

  // out of boundaries check
  float enter = 0.0;
  float leave = 0.0;

  while (cone_sample.a < 1.0f && t <= max_distance) {
    vec3 cone_position = position + direction * t;

    if(any(greaterThan(cone_position, vec3(1.0))) || any(lessThan(cone_position, vec3(0.0)))) {
      break;
    }

    // cone expansion and respective mip level based on diameter
    const float diameter = 2.0f * tan(aperture) * t;
    const float mip_level = max(log2(diameter / voxel_size), 1.0);

    // sample voxel structure at current position and mip level of cone
    vec4 current_sample = vxgi_sample_function(cone_position, weight, visible_faces, mip_level, VoxelTextureTypeRadiance);
    current_sample += vxgi_sample_function(cone_position, weight, visible_faces, mip_level, VoxelTextureTypeEmission);

    // front to back composition
    cone_sample += (1.0f - cone_sample.a) * current_sample;

    if(trace_occlusion && occlusion < 1.0)
    {
      occlusion += ((1.0f - occlusion) * cone_sample.a) / (1.0f + 1.0f * diameter);
    }

    // move further into volume
    if (vxgi_options.anisotropic) {
      t += diameter;
    } else {
      t += diameter / 3.0;
    }
  }

  return vec4(cone_sample.rgb, occlusion);
}

vec4 calculate_indirect_lighting(vec3 position, vec3 normal)
{
    // move position further along normal, 1 voxel step, half dimension
    position = position + normal * (1.0f / (vxgi_options.dimension / 2.0f));

    vec3 guide = vec3(0.0f, 1.0f, 0.0f);

    if (abs(dot(normal, guide)) == 1.0) {
        guide = vec3(0.0f, 0.0f, 1.0f);
    }

    const vec3 right = normalize(guide - dot(normal, guide) * normal);
    const vec3 up = normalize(cross(right, normal));

    vec4 diffuse = vec4(0.0f);
    for(int i = 0; i < 4; i++)
    {
        vec3 cone_direction = normal;
        cone_direction += propagationDirections[i].x * right + propagationDirections[i].z * up;
        cone_direction = normalize(cone_direction);

        diffuse += trace_cone(position, normal, cone_direction, 1.0, false) * diffuseConeWeights[i];
    }

    return clamp(diffuse, 0.0f, 1.0f);
}

void main()
{
    if(gl_GlobalInvocationID.x >= vxgi_options.dimension
    || gl_GlobalInvocationID.y >= vxgi_options.dimension 
    || gl_GlobalInvocationID.z >= vxgi_options.dimension) return;

    const ivec3 write_position = ivec3(gl_GlobalInvocationID);
    const vec4 albedo = texelFetch(voxel_albedo, write_position, 0);

    if(albedo.a < EPSILON) { return; }
    
    const vec3 position = vec3(write_position) / vxgi_options.dimension;
    const vec3 normal = texelFetch(voxel_normal, write_position, 0).xyz * 2.0 - 1.0;
    const vec4 direct_light = imageLoad(voxel_radiance, write_position);

    vec4 indirect_light = calculate_indirect_lighting(position, normal);
    indirect_light *= albedo;
    vec4 radiance = direct_light + indirect_light * 0.05;
    radiance.a = direct_light.a;

    imageStore(voxel_radiance, write_position, radiance);
}