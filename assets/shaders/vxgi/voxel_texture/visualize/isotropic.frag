#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1

#include vxgi/options.glsl
#include libs/camera.glsl

layout(location = 2) uniform uint dimension;
layout(location = 3) uniform uint level;

layout(binding = 4) uniform sampler3D texture;

layout(location = 0) in flat vec4 geometry_voxel_color;

layout (location = 0) out vec4 color;

void main()
{
    if (geometry_voxel_color.a < 0.0001) {
        discard;
    }

    color = vec4(geometry_voxel_color.rgb, 1.0);
}