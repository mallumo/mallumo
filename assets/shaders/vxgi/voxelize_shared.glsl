vec3 encode_normal(vec3 normal)
{
    return normal * 0.5f + vec3(0.5f);
}

vec3 decode_normal(vec3 normal)
{
    return normal * 2.0f - vec3(1.0f);
}

uint uvec3_to_uint(uvec3 val) {
    return (uint(val.z) & 0x000003FF)   << 20U
            |(uint(val.y) & 0x000003FF) << 10U 
            |(uint(val.x) & 0x000003FF);
}

uvec3 uint_to_uvec3(uint val) {
    return uvec3(uint((val & 0x000003FF)),
                 uint((val & 0x000FFC00) >> 10U), 
                 uint((val & 0x3FF00000) >> 20U));
}

void image_average_rgba8(layout(r32ui) volatile coherent restrict uimage3D grid, ivec3 coords, vec3 value) {
  uint nextUint = packUnorm4x8(vec4(value, 1.0f / 255.0f));
  uint prevUint = 0;
  uint currUint;

  vec4 currVec4;

  vec3 average;
  uint count;

  //"Spin" while threads are trying to change the voxel
  while ((currUint = imageAtomicCompSwap(grid, coords, prevUint, nextUint)) != prevUint) {
    prevUint = currUint;                 // store packed rgb average and count
    currVec4 = unpackUnorm4x8(currUint); // unpack stored rgb average and count

    average = currVec4.rgb;            // extract rgb average
    count = uint(currVec4.a * 255.0f); // extract count

    // Compute the running average
    average = (average * count + value) / (count + 1);

    // Pack new average and incremented count back into a uint
    nextUint = packUnorm4x8(vec4(average, (count + 1) / 255.0f));
  }
}

void image_average_rgba32(layout(r32ui) volatile coherent uimage3D lock_image,
                          layout(r32ui) volatile coherent uimage3D count_image,
                          layout(rgba32f) volatile coherent image3D protected_image, 
                          ivec3 position,
                          vec4 value) {

  bool done = false;
  uint locked = 0;
  uint tries = 64;

  while (!done && tries != 0) {
    locked = imageAtomicExchange(lock_image, position, 1U);
    if (locked == 0) {
      uint old_count = imageLoad(count_image, position).x;
      uint new_count = old_count + 1;

      vec4 old_value = imageLoad(protected_image, position);
      vec4 new_value = (old_value * old_count + value) / float(new_count);

      imageStore(protected_image, position, new_value);
      imageStore(count_image, position, uvec4(new_count));
      memoryBarrier();

      imageAtomicExchange(lock_image, position, 0U);

      done = true;
    }
    tries--;
  }
}