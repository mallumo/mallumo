#version 450 core

layout(std140, binding = 0) uniform Camera
{
    mat4 projection;
    mat4 view;
} camera;

in vertexOut {
    vec3 position;
} vertex_out;

layout (binding = 0) uniform samplerCubeArray environment_cubemap;

layout (location = 0) out vec4 color;

const float PI = 3.14159265359f;

void main()
{
    vec3 N = normalize(vertex_out.position);

    vec3 irradiance = vec3(0.0);
    
    // tangent space calculation from origin point
    vec3 up    = vec3(0.0, 1.0, 0.0);
    vec3 right = cross(up, N);
    up         = cross(N, right);
       
    float sampleDelta = 0.2f;
    float nrSamples = 0.0f;
    for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta * 2.0)
    {
        for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
        {
            // spherical to cartesian (in tangent space)
            vec3 tangent_sample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
            // tangent space to world
            vec3 sample_vec = tangent_sample.x * right + tangent_sample.y * up + tangent_sample.z * N; 

            irradiance += texture(environment_cubemap, vec4(sample_vec, 0)).rgb * cos(theta) * sin(theta);
            nrSamples++;
        }
    }
    irradiance = PI * irradiance * (1.0 / float(nrSamples));
    
    color = vec4(irradiance, 1.0);
}