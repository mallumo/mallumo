vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

///
/// Diffuse by disney [Brent Burley 2012]
///
vec3 diffuse_disney(vec3 diffuse, float roughness, float LdotH, float NdotV, float NdotL)
{
    float alpha = roughness*roughness;
    float f90 = 2.0 * LdotH * LdotH * alpha - 0.5;

    return (diffuse / PI) * (1.0 + f90 * pow((1.0 - NdotL), 5.0)) * (1.0 + f90 * pow((1.0 - NdotV), 5.0));
}

///
/// Final Cook-Torrance BRDF
///     DFG
/// -----------
/// 4(ωo⋅n)(ωi⋅n)
///
vec3 brdf(vec3 N, vec3 H, vec3 V, vec3 L, float roughness, vec3 f0)
{
    float D = cook_torrance_distribution(N, H, roughness);
    vec3  F = cook_torrance_fresnel(max(dot(H, V), 0.0), f0);
    float G = cook_torrance_geometry(N, V, L, roughness);

    vec3  nominator   = D * F * G;
    float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; 
    
    return nominator / denominator;
}

vec3 irradiance(vec3 N, vec3 H, vec3 V, vec3 L, vec3 radiance, vec4 albedo, float roughness, float metalness)
{
    // Approximation of F0 for Fresnel
    vec3 f0 = mix(vec3(0.04, 0.04, 0.04), albedo.rgb, metalness);
    vec3 F = fresnelSchlick(max(dot(H, V), 0.0), f0);
    vec3 brdf = brdf(N, H, V, L, roughness, f0);

    // kS = energy that gets reflected
    // kD = energy that gets refracted ( 1.0 - reflected )
    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS; 

    // nullify kD if the surface is metallic
    // metallic surface does not refract light
    kD *= 1.0 - metalness;

    // light's outgoing reflectance
    // from reflectance equation
    // (                           )
    // ( kD * diffuse + kS * BRDF  ) * radiance * N ⋅ L
    // (                           )
    float NdotL = max(dot(N, L), 0.0);

    vec3 diffuse = diffuse_disney(albedo.rgb, roughness, dot(L, H), dot(N, V), dot(N, L));

    return (kD * diffuse / PI + brdf) * radiance * NdotL;
}
