#version 450 core

#define NODE_MASK_VALUE 0x3FFFFFFF
#define NODE_NOT_FOUND 0xFFFFFFFF

#define TSQRT2 2.828427
#define SQRT2 1.414213
#define ISQRT2 0.707106

const float PI = 3.14159265359;

const uvec3 child_offsets[8] = {
  uvec3(0, 0, 0),
  uvec3(1, 0, 0),
  uvec3(0, 1, 0),
  uvec3(1, 1, 0),
  uvec3(0, 0, 1),
  uvec3(1, 0, 1),
  uvec3(0, 1, 1),
  uvec3(1, 1, 1)
};

in vertexOut {
  vec2 texture_coordinate;
} vertex_out;

layout(std140, binding = 0) uniform Globals
{
    uint grid_size;
    float voxel_size;
    uint levels;
} globals;

layout(std140, binding = 1) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;


struct Light {
    vec4 pos_far;
    vec3 color;
    uint has_shadowmap;
};

layout(std430, binding = 0) buffer NodePoolNext
{
  uint nodepool_next[];
};

layout(std430, binding = 1) buffer LightParameters
{
    Light lights[];
} light_parameters;

layout(r32ui, binding = 0) uniform uimage3D brickpool_irradiance;

layout(binding = 1) uniform sampler2D depth_texture;
layout(binding = 2) uniform sampler2D position_texture;
layout(binding = 3) uniform sampler2D albedo_texture;
layout(binding = 4) uniform sampler2D orm_texture;
layout(binding = 5) uniform sampler2D normal_texture;
layout(binding = 6) uniform sampler2D emission_texture;

layout(binding = 7) uniform samplerCubeArray shadowmaps[4];

layout (location = 0) out vec4 color;

uint traverse_octree_lod(
    in vec3 position, 
    in uint target_level
) {  
  vec3 node_position_min = vec3(1.0);
  vec3 node_position_max = vec3(1.0);

  float side_length = 1.0;
  uint node_address = 0;

  for (uint iLevel = 0; iLevel < target_level; ++iLevel) {
    uint nodeNext = nodepool_next[node_address];

    uint child_start_address = nodeNext & NODE_MASK_VALUE;    
    if (child_start_address == 0U) {
        node_address = uint(NODE_NOT_FOUND);
        break;
    }
    
    uvec3 offset_vec = uvec3(2.0 * position);
    uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;
    
    node_address = int(child_start_address + offset);
    node_position_min += vec3(child_offsets[offset]) * vec3(side_length);
    node_position_max = node_position_min + vec3(side_length);

    side_length = side_length / 2.0;
    position = 2.0 * position - vec3(offset_vec);
  }

  return node_address;
}


// Scales and bias a given vector (i.e. from [-1, 1] to [0, 1]).
vec3 scaleAndBias(const vec3 p) {
    return 0.5f * p + vec3(0.5f); 
}

// Returns a vector that is orthogonal to u.
vec3 orthogonal(vec3 u){
	u = normalize(u);
	vec3 v = vec3(0.99146, 0.11664, 0.05832); // Pick any normalized vector.
	return abs(dot(u, v)) > 0.99999f ? cross(u, vec3(0, 1, 0)) : cross(u, v);
}

// Traces a diffuse voxel cone.
vec3 traceDiffuseVoxelCone(const vec3 from, vec3 direction){
	direction = normalize(direction);
	
	const float CONE_SPREAD = 0.325;

	vec4 acc = vec4(0.0f);

	// Controls bleeding from close surfaces.
	// Low values look rather bad if using shadow cone tracing.
	// Might be a better choice to use shadow maps and lower this value.
	float dist = 0.1953125;

	// Trace.
	while(dist < SQRT2 && acc.a < 1.0){
		vec3 c = from + dist * direction;
		c = scaleAndBias(from + dist * direction);
		float l = (1 + CONE_SPREAD * dist / globals.voxel_size);
		float level = log2(l);
		float ll = (level + 1) * (level + 1);

		//vec4 voxel = textureLod(texture3D, c, min(MIPMAP_HARDCAP, level));
        uint node_address = traverse_octree_lod(c, uint(min(1, globals.levels - 1 - level)));
        if(node_address != NODE_NOT_FOUND) {   
            vec4 voxel = vec4(0.0);         
            ivec3 brick_coordinates = ivec3(
                3 * (node_address % 512),
                3 * ((node_address / 512) % 512),
                3 * (node_address / (512 * 512))
            );
            for(uint x = 0; x < 3; x++) {
                for(uint y = 0; y < 3; y++) {
                    for(uint z = 0; z < 3; z++) {
                        uint value = imageLoad(brickpool_irradiance, brick_coordinates + ivec3(x, y, z)).r;
                        voxel += unpackUnorm4x8(value);
                    }
                }
            }
            voxel /= 27.0;
            voxel.a = 0.011;

            acc += 0.075 * ll * voxel /* * pow(1 - voxel.a, 2) */;
        }
        
		dist += ll * globals.voxel_size * 2;
	}
	return pow(acc.rgb * 2.0, vec3(1.5));
}

vec3 indirectDiffuseLight(vec3 frag_pos, vec3 normal) {
	const float ANGLE_MIX = 0.5f; // Angle mix (1.0f => orthogonal direction, 0.0f => direction of normal).

	const float w[3] = {1.0, 1.0, 1.0}; // Cone weights.

	// Find a base for the side cones with the normal as one of its base vectors.
	const vec3 ortho = normalize(orthogonal(normal));
	const vec3 ortho2 = normalize(cross(ortho, normal));

	// Find base vectors for the corner cones too.
	const vec3 corner = 0.5f * (ortho + ortho2);
	const vec3 corner2 = 0.5f * (ortho - ortho2);

	// Find start position of trace (start with a bit of offset).
	const vec3 N_OFFSET = normal * (1 + 4 * ISQRT2) * globals.voxel_size;
	const vec3 C_ORIGIN = frag_pos + N_OFFSET;

	// Accumulate indirect diffuse light.
	vec3 acc = vec3(0);

	// We offset forward in normal direction, and backward in cone direction.
	// Backward in cone direction improves GI, and forward direction removes
	// artifacts.
	const float CONE_OFFSET = -0.01;

	// Trace front cone
	acc += w[0] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * normal, normal);

	const vec3 s1 = mix(normal, ortho, ANGLE_MIX);
	const vec3 s2 = mix(normal, -ortho, ANGLE_MIX);
	const vec3 s3 = mix(normal, ortho2, ANGLE_MIX);
	const vec3 s4 = mix(normal, -ortho2, ANGLE_MIX);

	acc += w[1] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * ortho, s1);
	acc += w[1] * traceDiffuseVoxelCone(C_ORIGIN - CONE_OFFSET * ortho, s2);
	acc += w[1] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * ortho2, s3);
	acc += w[1] * traceDiffuseVoxelCone(C_ORIGIN - CONE_OFFSET * ortho2, s4);

	const vec3 c1 = mix(normal, corner, ANGLE_MIX);
	const vec3 c2 = mix(normal, -corner, ANGLE_MIX);
	const vec3 c3 = mix(normal, corner2, ANGLE_MIX);
	const vec3 c4 = mix(normal, -corner2, ANGLE_MIX);

	acc += w[2] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * corner, c1);
	acc += w[2] * traceDiffuseVoxelCone(C_ORIGIN - CONE_OFFSET * corner, c2);
	acc += w[2] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * corner2, c3);
	acc += w[2] * traceDiffuseVoxelCone(C_ORIGIN - CONE_OFFSET * corner2, c4);

	// Return result.
	return acc;
    //return vec3(1.0, 0.0, 0.0);
}

///
/// Normal Distribution Function
///
///                                   α^2
/// Trowbridge-Reitz GGX = ----------------------------
///                        π( (n⋅h)^2 * (α^2 − 1) + 1)^2
///
/// N - normal vector 
/// H - half-way vector
/// a - α = roughness
///
float distributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float nom    = a2;
    float denom  = (NdotH2 * (a2 - 1.0) + 1.0);
    denom        = PI * denom * denom;
	
    return nom / denom;
}

/// 
/// Geometry Function
/// 
///                    n⋅v
/// SchlickGGX = ---------------
///                (n⋅v)(1−k)+k
/// 
/// k - remapping of α based on whether one uses direct or IBL lighting
///     for direct k = (α + 1)^2 / 8
///     for IBL    k = (α^2) / 2
/// N ⋅ V
///
float geometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

/// 
/// Smith's method for taking into account the view direction(geometry obstruction) 
/// and the light direction vector(geometry shadowing)
/// 
/// G(n, v, l, k) = SchlickGGX(n, v, k) * SchlickGGX(n, l, k)
/// 
float geometrySmith(vec3 N, vec3 V, vec3 L, float k)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx1 = geometrySchlickGGX(NdotV, k);
    float ggx2 = geometrySchlickGGX(NdotL, k);
	
    return ggx1 * ggx2;
}

///
/// Fresnel-Schlick approximation
///
/// f0 - base reflectivity of the surface
/// N ⋅ V
///
vec3 fresnelSchlick(float NdotV, vec3 f0)
{
    return f0 + (1.0 - f0) * pow(1.0 - NdotV, 5.0);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}   

///
/// Shadow calculations
///
vec3 grid_sampling_disk[20] = vec3[]
(
   vec3(1, 1, 1), vec3(1, -1, 1), vec3(-1, -1, 1), vec3(-1, 1, 1), 
   vec3(1, 1, -1), vec3(1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1, 0), vec3(1, -1, 0), vec3(-1, -1, 0), vec3(-1, 1, 0),
   vec3(1, 0, 1), vec3(-1, 0, 1), vec3(1, 0, -1), vec3(-1, 0, -1),
   vec3(0, 1, 1), vec3(0, -1, 1), vec3(0, -1, -1), vec3(0, 1, -1)
);

float in_shadow(vec3 frag_pos, Light light, uint shadowmap_index)
{
    // Get vector between fragment position and light position
    vec3 frag_to_light = frag_pos - light.pos_far.xyz;
    // Get current linear depth as the length between the fragment and light position
    float current_depth = length(frag_to_light);
    // Test for shadows with PCF
    float shadow = 0.0;
    int samples = 20;
    float view_distance = length(camera.position.xyz - frag_pos);
    float bias = 0.005;
    float disk_radius = (1.0 + (view_distance / light.pos_far.w)) / 700.0;
    for(int i = 0; i < samples; ++i)
    {
        float closest_depth = texture(shadowmaps[shadowmap_index], vec4(frag_to_light + grid_sampling_disk[i] * disk_radius, 0.0)).r;
        closest_depth *= light.pos_far.w;   // Undo mapping [0;1]
        if(abs(current_depth - bias) > closest_depth)
            shadow += 1.0;
    }
    shadow /= float(samples);
    
    return shadow;
}

void main() {
    vec2 uv = vertex_out.texture_coordinate;

    float depth = texture(depth_texture, uv).r;
    if (depth > 0.9999) {
        discard;
    }

    vec3 position = texture(position_texture, uv).xyz;

    vec4 albedo = texture(albedo_texture, uv);

    if (albedo.a < 0.0001) {
        discard;
    }

    vec3 orm = texture(orm_texture, uv).rgb;
    float occlusion = orm.r;
    float roughness = orm.g;
    float metalness = orm.b;

    vec3 normal = texture(normal_texture, uv).xyz;

    vec3 emission = texture(emission_texture, uv).xyz;
    
    // Approximation of F0 for Fresnel
    vec3 f0 = mix(vec3(0.04, 0.04, 0.04), albedo.rgb, metalness);

    vec3 N = normal;
    vec3 V = normalize(vec3(camera.position) - position);
    vec3 R = reflect(-V, N); 

    vec3 Lo = vec3(0.0);
    for(uint i = 0; i < light_parameters.lights.length(); i++) {
        vec3 light_position = light_parameters.lights[i].pos_far.xyz;
        vec3 light_color = light_parameters.lights[i].color;

        vec3 L = normalize(light_position - position); 
        vec3 H = normalize(V + L);

        // Radiance
        float distance    = length(light_position - position);
        float attenuation = 1.0 / (distance * distance); // inverse-square law
        vec3 radiance     = light_color * attenuation;

        // Final Cook-Torrance BRDF
        //     DFG
        // -----------
        // 4(ωo⋅n)(ωi⋅n)
        float D = distributionGGX(N, H, roughness);
        float G = geometrySmith(N, V, L, roughness);
        vec3  F = fresnelSchlick(max(dot(H, V), 0.0), f0);

        vec3  nominator   = D * G * F;
        float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; 
        vec3  brdf        = nominator / denominator; 

        // kS = energy that gets reflected
        // kD = energy that gets refracted ( 1.0 - reflected )
        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS; 

        // nullify kD if the surface is metallic
        // metallic surface does not refract light
        kD *= 1.0 - metalness;

        // light's outgoing reflectance
        // from reflectance equation
        // (      c              )
        // ( kD * - + kS * BRDF  ) * radiance * N ⋅ L
        // (      π              )
        float NdotL = max(dot(N, L), 0.0);

        float shadow = light_parameters.lights[i].has_shadowmap == 0
                       ? 0.0
                       : in_shadow(position, light_parameters.lights[i], i);
        //float shadow = 0.0;
        Lo += (1.0 - shadow) * ((kD * albedo.rgb / PI + brdf) * radiance * NdotL);
    }

    // svogi
    vec3 kD = vec3(1.0);
    Lo = Lo + emission;

    color = vec4(indirectDiffuseLight(position, normal), 1.0);
}