#version 450 core

in vertexOut {
  vec2 texture_coordinate;
} vertex_out;

layout(binding = 0) uniform sampler2D previous_frame;
layout(binding = 1) uniform sampler2D sub_frame;

layout(location = 0) uniform mat4 projection_matrix;
layout(location = 1) uniform mat4 inverse_projection_matrix;
layout(location = 2) uniform mat4 previous_view_matrix;
layout(location = 3) uniform mat4 inverse_view_matrix;

layout(location = 4) uniform vec2 frame_size;
layout(location = 5) uniform vec2 sub_frame_size;
layout(location = 6) uniform float sub_pixel_size;
layout(location = 7) uniform float sub_frame_number;

layout (location = 0) out vec4 color;

void main()
{
    vec2 texture_coordinate = floor(vertex_out.texture_coordinate * frame_size);
    
    float x = mod(texture_coordinate.x, sub_pixel_size);
    float y = mod(texture_coordinate.y, sub_pixel_size);
    float fragment_frame_number = y * sub_pixel_size + x;

    if(fragment_frame_number == sub_frame_number)
    {
        vec2 texture_coordinate_sub_frame = (floor(vertex_out.texture_coordinate * sub_frame_size) + 0.5) / sub_frame_size;
        color = texture(sub_frame, texture_coordinate_sub_frame);
        return;
    } 

    mat3 reduced_previous_view_matrix = mat3(previous_view_matrix);
    mat3 reduced_inverse_view_matrix = mat3(inverse_view_matrix);

    vec4 previous_pos = vec4(vertex_out.texture_coordinate * 2.0 - 1.0, 1.0, 1.0);
    previous_pos = inverse_projection_matrix * previous_pos;
    previous_pos = previous_pos / previous_pos.w;
    previous_pos.xyz = reduced_inverse_view_matrix * previous_pos.xyz;
    previous_pos.xyz = reduced_previous_view_matrix * previous_pos.xyz;

    vec4 reprojected = projection_matrix * previous_pos;
    reprojected /= reprojected.w;
    
    if( reprojected.y < -1.0 || reprojected.y > 1.0 || reprojected.x < -1.0 || reprojected.x > 1.0)
    {
        color = texture(sub_frame, vertex_out.texture_coordinate);
    }
    else
    {
        reprojected.xy = reprojected.xy * 0.5 + 0.5;
        color = texture(previous_frame, reprojected.xy);
    }
}