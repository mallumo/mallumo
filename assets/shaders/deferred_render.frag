#version 450 core

in vertexOut {
  vec2 texture_coordinate;
} vertex_out;

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

struct Light {
    vec4 pos_far;
    vec3 color;
    uint has_shadowmap;
};

layout(std430, binding = 0) buffer LightParameters
{
    Light lights[];
} light_parameters;

layout(location = 0) uniform sampler3D tex;/*
layout(location = 1) uniform sampler2D position_texture;
layout(location = 2) uniform sampler2D albedo_texture;
layout(location = 3) uniform sampler2D orm_texture;
layout(location = 4) uniform sampler2D normal_texture;
layout(location = 5) uniform sampler2D emission_texture;*/

layout (location = 0) out vec4 color;

void main()
{
    color = vec4(texture(tex, vec3(vertex_out.texture_coordinate, 0.5)).rgb, 1.0);
}