in vertexOut {
    vec2 texture_coordinate;
} vertex_out;

layout(std140, binding = 0) uniform Camera 
{
    mat4 projection_matrix;
    mat4 view_matrix;
    mat4 projection_view_matrix;
    vec4 position;
} camera;

layout(location = 1) uniform vec2 frame_size;
layout(location = 2) uniform float pixel_size;
layout(location = 3) uniform float frame_number;

layout(location = 4) uniform float max_iterations;

layout(location = 5) uniform vec3 base_offset = vec3(0.0, 0.0, 0.0);
layout(location = 6) uniform float base_scale = 0.00025;

layout(location = 7) uniform vec3 detail_offset = vec3(0.0, 0.0, 0.0);
layout(location = 8) uniform float detail_scale = 8.0;

layout(location = 9) uniform vec2 curl_offset = vec2(0.0, 0.0);
layout(location = 10) uniform float curl_scale = 0.5;
layout(location = 11) uniform float curl_scalar = 0.35;

layout(location = 12) uniform vec2 coverage_offset = vec2(0.0, 0.0);
layout(location = 13) uniform float coverage_scale = 0.0000175;

// clouds
layout(binding = 0) uniform sampler3D perlin_worley_noise_texture;
layout(binding = 1) uniform sampler3D worley_noise_texture;
layout(binding = 2) uniform sampler2D curl_noise_texture;
layout(binding = 3) uniform sampler2D coverage_texture;

// atmosphere
layout(location = 0) uniform vec3 sun_direction;
layout(binding = 4) uniform sampler2D transmittance_texture;
layout(binding = 5) uniform sampler3D scattering_texture;
layout(binding = 6) uniform sampler2D irradiance_texture;

layout (location = 0) out vec4 color;

// screen_position:
// 
// 0.0, 1.0       1.0, 1.0
//
//
// 0.0, 0.0       1.0, 0.0
vec3 ray_direction(vec2 screen_position) {
    vec2 ndc = screen_position * 2.0 - 1.0;

    vec4 ray_clip = vec4(ndc, -1.0, 1.0);
    
    vec4 ray_eye = inverse(camera.projection_matrix) * ray_clip;
    ray_eye = vec4(ray_eye.xy, -1.0, 0.0);

    return normalize((inverse(camera.view_matrix) * ray_eye).xyz);
}

const float SAMPLE_TRESHOLD = 0.15;

const float SAMPLE_LIGHT_MAX_ITERATIONS = 10;
const float SUN_RAY_LENGTH = 1000.0;

const float ATMOSPHERE_START_HEIGHT = 1500.0;
const float ATMOSPHERE_END_HEIGHT = 4000.0;
const float ATMOSPHERE_THICKNESS = ATMOSPHERE_END_HEIGHT - ATMOSPHERE_START_HEIGHT;

const float DENSITY_SCALAR = 1.0 / 3.0;
const float FORWARD_MIE_SCATTERING_G = 0.8;
const float BACKWARD_MIE_SCATTERING_G = -0.4;

const float EROSION_EDGE_SIZE = 0.2;

const vec4 STRATUS_GRADIENT = vec4(0.01f, 0.095f, 0.125f, 0.225f);
const vec4 CUMULUS_GRADIENT = vec4(0.0f, 0.095f, 0.31f, 0.50f);
const vec4 CUMULONUMBIS_GRADIENT = vec4(0.0f, 0.085f, 0.75f, 1.0f);

const float EARTH_CLOUDS_CENTRE = 407580.0;

const float HORIZON_FADE_ALPHA = 0.75;
const float HORIZON_FADE_SCALAR = 0.185;

// slightly cheaper phase function
float henyey_greenstein_phase_function(float g, float nu) {
    float g2 = g * g;
    return (1.0 - g2) / pow(1.0 + g2 - 2.0 * g * nu, 1.5);
}

float beer_term(float density) {
    return exp(-DENSITY_SCALAR * density);
}

float powder_term(float density, float cosTheta) {
    float powder = 1.0 - exp(-DENSITY_SCALAR * density * 2.0);
    powder = clamp(powder * 2.0, 0.0, 1.0);
    return mix(1.0, powder, smoothstep(0.5, -0.5, cosTheta));
}

vec3 ray_sphere_intersect(float sphereRadius, vec3 origin, vec3 direction) {
	  float a0 = sphereRadius * sphereRadius - dot(origin, origin);
	  float a1 = dot(origin, direction);
	  float result = sqrt(a1 * a1 + a0) - a1;
	
	  return origin + direction * result;
}

float normalized_altitude(vec3 ray) {
    return (length(ray) - EARTH_CLOUDS_CENTRE - ATMOSPHERE_START_HEIGHT) / ATMOSPHERE_THICKNESS;
}

float gradient_step(float a, vec4 gradient) {
    return smoothstep(gradient.x, gradient.y, a) - smoothstep(gradient.z, gradient.w, a);
}

float smooth_threshold(float value, float threshold, float edge_size) {
    return smoothstep(threshold, threshold + edge_size, value);
}

vec3 smooth_threshold(vec3 value, float threshold, float edge_size) {
    value.r = smoothstep(threshold, threshold + edge_size, value.r);
    value.g = smoothstep(threshold, threshold + edge_size, value.g);
    value.b = smoothstep(threshold, threshold + edge_size, value.b);
  
    return value;
}

float mix3(float v0, float v1, float v2, float a) {
    return a < 0.5 ? mix(v0, v1, a * 2.0) : mix(v1, v2, (a-0.5) * 2.0);
}

vec4 mix3(vec4 v0, vec4 v1, vec4 v2, float a) {
    return vec4(
        mix3(v0.x, v1.x, v2.x, a),
        mix3(v0.y, v1.y, v2.y, a),
        mix3(v0.z, v1.z, v2.z, a),
        mix3(v0.w, v1.w, v2.w, a));
}

float sample_cloud(vec3 point) {
    float norm_altitude = normalized_altitude(point);

    if (norm_altitude > 1.0) {
        return 0.0;
    }

    vec3 coverage = textureLod(coverage_texture, point.xz * coverage_scale * 0.5 + 0.5 + coverage_offset, 0.0).rgb;

    float gradient = gradient_step(
        norm_altitude,
        mix3(CUMULONUMBIS_GRADIENT, CUMULONUMBIS_GRADIENT, STRATUS_GRADIENT, coverage.b)
    );

    if (coverage.r < 0.001 || gradient < 0.001) {
        return 0.0;
    }

    float value = 0.0;
    vec3 coord = vec3(point * base_scale + base_offset);
    vec4 noise_sample = textureLod(perlin_worley_noise_texture, coord.xyz, 0.0);
    vec4 gradient_scalar = vec4(
        1.0,
        gradient_step(norm_altitude, STRATUS_GRADIENT),
        gradient_step(norm_altitude, CUMULUS_GRADIENT),
        gradient_step(norm_altitude, CUMULONUMBIS_GRADIENT)
    );

    noise_sample *= gradient_scalar;

    float noise = clamp((noise_sample.r + noise_sample.g + noise_sample.b + noise_sample.a) / 4.0, 0.0, 1.0);
    noise *= gradient;

    noise = smooth_threshold(noise, SAMPLE_TRESHOLD, EROSION_EDGE_SIZE);
    noise = clamp(noise - (1.0 - coverage.r), 0.0, 1.0) * coverage.r;
    
    if (noise > 0.0 && noise < 1.0) {
        vec2 dist_uv = vec2(point.xy * base_scale * curl_scale);
        dist_uv += curl_offset;
        vec3 curl = textureLod(curl_noise_texture, dist_uv, 0.0).rgb * 2.0 - 1.0;

        coord = vec3(point * base_scale * detail_scale);
        coord += detail_offset;

        curl *= curl_scalar * norm_altitude;
        coord += curl;

        vec3 detail = 1.0 - textureLod(worley_noise_texture, coord, 0.0).rgb;
        detail *= gradient_scalar.gba;
        float detailValue = (detail.r + detail.g + detail.b) / 3.0;
        detailValue *= smoothstep(1.0, 0.0, noise) * 0.5;
        noise -= detailValue;

        noise = clamp(noise, 0.0, 1.0);
    }

    return noise;
}

vec3 sample_light(vec3 original_sample_point, float original_sample_density, float nu) {
    vec3 ray_step = sun_direction * (SUN_RAY_LENGTH / SAMPLE_LIGHT_MAX_ITERATIONS);
    vec3 sample_point = original_sample_point + ray_step;
    
    float norm_altitude = 0.0;

    float thickness = 0.0;

    for(float i = 0.0; i < SAMPLE_LIGHT_MAX_ITERATIONS; i++) {
        sample_point += ray_step;
        thickness += sample_cloud(sample_point);
    }

    sample_point += ray_step * 8.0;
    thickness += sample_cloud(sample_point);

    float forward_p = henyey_greenstein_phase_function(FORWARD_MIE_SCATTERING_G, nu);
    float backwards_p = henyey_greenstein_phase_function(BACKWARD_MIE_SCATTERING_G, nu);
    float phase = (forward_p + backwards_p) / 2.0;

    vec3 clouds_position = original_sample_point - vec3(0.0, EARTH_CLOUDS_CENTRE, 0.0);

    vec3 point = to_earth_space(clouds_position, false);
    float r = length(point);
    vec3 sun_dir = sun_direction;
    float mu_s = dot(point, sun_dir) / r;
    vec3 clouds_sky_illuminance = globals.parameters.sky_spectral_radiance_to_luminance * get_irradiance(globals.parameters, irradiance_texture, r, mu_s);

    vec3 clouds_sun_illuminance =
        globals.parameters.sun_spectral_radiance_to_luminance *
        globals.parameters.solar_irradiance *
        get_transmittance_to_sun(globals.parameters, transmittance_texture, r, mu_s);

    vec3 clouds_luminance = (1.0 / (PI * PI)) * (clouds_sun_illuminance + clouds_sky_illuminance);

    vec3 clouds_transmittance;
    vec3 clouds_in_scatter = get_sky_luminance_to_point(camera.position.xyz, clouds_position, sun_direction, clouds_transmittance, transmittance_texture, scattering_texture);
    
    vec3 color = adjust_exposure(clouds_luminance * clouds_transmittance + clouds_in_scatter);

    if (color.b != 0.0) {
        color *= (color.r * color.g) / color.b;
    }

    color = color * phase * beer_term(thickness) * powder_term(original_sample_density, nu);
    color += max(dot(vec3(0.0, 1.0, 0.0), sun_direction), 0.0) * mix(vec3(0.507,0.754,1.0), vec3(1.0), normalized_altitude(original_sample_point));

    return max(1.0 - textureLod(coverage_texture, original_sample_point.xz * coverage_scale * 0.5 + 0.5 + coverage_offset, 0.0).g, 0.05) * color;
}

void main() {
    float x = mod(frame_number, pixel_size);
    float y = floor(frame_number / pixel_size);

    vec3 camera_clouds = camera.position.xyz + vec3(0.0, EARTH_CLOUDS_CENTRE, 0.0);

    vec2 texture_coordinate_offset = vec2(x, y) / frame_size;

    vec3 ray_direction = ray_direction(vertex_out.texture_coordinate + texture_coordinate_offset);
    float ray_step_length = 5120.0 / max_iterations;

    vec4 final_color = vec4(0.0);

    vec3 point = to_earth_space(camera.position.xyz, false);
    float r = length(point);
    float mu = dot(point, ray_direction) / r;

    bool intersects_ground = ray_intersects_ground(globals.parameters, r, mu);

    if(!intersects_ground) {
        vec2 uv = vertex_out.texture_coordinate;
        vec3 sample_point = ray_sphere_intersect(EARTH_CLOUDS_CENTRE + ATMOSPHERE_START_HEIGHT, camera_clouds, ray_direction);
        
        vec3 ray_step = ray_direction * ray_step_length;
        
        float norm_altitude = 0.0;
        float transmittance = 1.0;

        float nu = dot(ray_direction, sun_direction);

        int i = 0;
        while(i < max_iterations && final_color.a < 1.0 && norm_altitude < 1.0)
        {
            float density = sample_cloud(sample_point);
            if(density > 0.0) {
                vec4 particle = vec4(density, density, density, density);
                float sample_transmittance = 1.0 - particle.a;
                transmittance *= sample_transmittance;            

                particle.a = 1.0 - sample_transmittance;
                particle.rgb = sample_light(sample_point, particle.a, nu) * particle.a;

                final_color += (1.0 - final_color.a) * particle;
            }

            i++;
            sample_point += ray_step;
            norm_altitude = normalized_altitude(sample_point);
        }
      
        float fade = smoothstep(0.0, HORIZON_FADE_SCALAR, ray_direction.y);
        final_color *= HORIZON_FADE_ALPHA + fade * (1.0 - HORIZON_FADE_ALPHA);
    }

    color = final_color;
}
