#version 450 core

out gl_PerVertex
{
  vec4 gl_Position;
};

struct Vertex {
    vec4 position;
    vec4 normal;
    vec4 texture_coordinate;
    vec4 tangent;
};

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

layout(std430, binding = 0) buffer Indices
{
    int indices[];
} indices;

layout(std430, binding = 1) buffer VertexAttributes
{
    Vertex va[];
} vertexAttributes;

layout(std430, binding = 2) buffer PrimitiveParameters {
  mat4 model_matrix;                   // 64B

  // Material
  vec4 albedo;                         // 16B
  vec4 metallic_roughness_refraction;  // 16B
  vec4 emission;                       // 16B

  // Material Textures
  uint has_albedo_texture;             // 4B
  uint has_metallic_roughness_texture; // 4B
  uint has_occlusion_texture;          // 4B
  uint has_normal_texture;             // 4B
  uint has_emissive_texture;           // 4B

  // Image based lighting
  uint has_diffuse_ibl;                // 4B
  uint has_specular_ibl;               // 4B

  // Shadows
  uint casts_shadows;                  // 4B
  uint receives_shadows;               // 4B

  // Padding
  uint padding[27];                   // 148B padded to 256
} primitive_parameters;

out vertexOut
{
    flat uint casts_shadows;
} vertex_out;

void main()
{
    int index = indices.indices[gl_VertexID];

    vec3 position = vertexAttributes.va[index].position.xyz;

    mat4 model_matrix = primitive_parameters.model_matrix;

    gl_Position = model_matrix * vec4(position, 1.0);
    vertex_out.casts_shadows = primitive_parameters.casts_shadows;
} 