layout(rgba8, binding = 0) uniform image3D input_texture;

void main()
{
    ivec3 loc = ivec3(
        gl_VertexID % imageSize(input_texture).x,
        (gl_VertexID / imageSize(input_texture).x) % imageSize(input_texture).y,
        (gl_VertexID / (imageSize(input_texture).x * imageSize(input_texture).y)) % imageSize(input_texture).z
    );

    vec3 prev = imageLoad(input_texture, loc).rgb;

    if (prev.r + prev.g + prev.b < 0.0001) {
        imageStore(input_texture, loc, vec4(loc, 0.0));
    } else {
        imageStore(input_texture, loc, vec4(loc, 1.0));
    }
}