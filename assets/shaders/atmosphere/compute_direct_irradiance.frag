layout(location = 0) out vec3 delta_irradiance;
layout(location = 1) out vec3 irradiance;

layout(binding = 0) uniform sampler2D transmittance_texture;

void main() {
    vec2 uv = gl_FragCoord.xy / IRRADIANCE_TEXTURE_SIZE;

    float r = globals.parameters.bottom_radius +
        tex_coord_to_unit_range(uv.y, IRRADIANCE_TEXTURE_HEIGHT) *
        (globals.parameters.top_radius - globals.parameters.bottom_radius);
    float mu_s = clamp(2.0 * tex_coord_to_unit_range(uv.x, IRRADIANCE_TEXTURE_WIDTH) - 1.0, -1.0, 1.0);
    float a_s = globals.parameters.sun_angular_radius;
    float average_cosine_factor;
    if (mu_s < -a_s) {
        average_cosine_factor = 0.0;
    } else if (mu_s > a_s) {
        average_cosine_factor = mu_s;
    } else {
        average_cosine_factor = (mu_s + a_s) * (mu_s + a_s) / (4.0 * a_s);
    }

    delta_irradiance =
        globals.parameters.solar_irradiance * 
        get_transmittance_to_top_atmospheric_boundary(globals.parameters, transmittance_texture, r, mu_s) *
        average_cosine_factor;
    irradiance = vec3(0.0);
}
