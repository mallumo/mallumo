struct DensityProfileLayer {
    float width;
    float exp_term;
    float exp_scale;
    float linear_term;
    float constant_term;
};

struct DensityProfile {
    DensityProfileLayer layers[2];
};

struct AtmosphereParameters {
    vec3 solar_irradiance;
    vec3 rayleigh_scattering;
    vec3 mie_scattering;
    vec3 mie_extinction;
    vec3 absorption_extinction;
    vec3 ground_albedo;
    vec3 sky_spectral_radiance_to_luminance;
    vec3 sun_spectral_radiance_to_luminance;

    mat3 luminance_from_radiance;

    DensityProfile rayleigh_density;
    DensityProfile mie_density;
    DensityProfile absorption_density;

    float sun_angular_radius;
    float bottom_radius;
    float top_radius;
    float mu_s_min;
    float mie_phase_function_g;

    int layer;
    int scattering_order;
};
