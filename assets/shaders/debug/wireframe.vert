#extension GL_ARB_shader_draw_parameters : enable

#define gl_DrawID gl_DrawIDARB

// include positions.glsl

out gl_PerVertex
{
  vec4 gl_Position;
};

struct Vertex {
    vec4 position;
};

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 camera_position;
} camera;

layout(std430, binding = INDICES_POSITION) buffer Indices
{
    int indices[];
} indices;

layout(std430, binding = VERTICES_POSITION) buffer VertexAttributes
{
    Vertex va[];
} vertexAttributes;

out VertexOut {
    vec3 barycentric;
} vertex_out;

void main(void)
{
  const vec3 barycentric[3] = vec3[3](vec3(1.0, 0.0, 0.0),
                                      vec3(0.0, 1.0, 0.0),
                                      vec3(0.0, 0.0, 1.0));

  int index = indices.indices[gl_VertexID];

  vec3 position = vertexAttributes.va[index].position.xyz * 0.5 + 0.5;

  vertex_out.barycentric = barycentric[gl_VertexID % 3];

  gl_Position = camera.projection_view_matrix * vec4(position, 1.0f);
}