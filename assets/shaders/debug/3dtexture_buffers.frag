#version 450 core

#extension GL_ARB_bindless_texture : enable

layout (location = 0) out vec4 color;

in geometryOut {
  vec4 color;
} geometry_out;

void main()
{
    color = vec4(geometry_out.color.rgb, 1.0);
}