#version 450 core

#define INDICES_BINDING 1
#define VERTICES_BINDING 2
#define PRIMITIVE_PARAMETERS_BINDING 3

#include libs/consts.glsl

#include libs/vertices.glsl
#include libs/parameters.glsl

#include libs/cook_torrance_distribution.glsl
#include libs/cook_torrance_fresnel.glsl
#include libs/cook_torrance_geometry.glsl
#include libs/pbr.glsl

#include libs/filmic_tonemapping.glsl

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

layout(binding = 4) uniform sampler2D albedo_sampler;
layout(binding = 5) uniform sampler2D metallic_roughness_sampler;
layout(binding = 6) uniform sampler2D occlusion_sampler;
layout(binding = 7) uniform sampler2D normal_sampler;
layout(binding = 8) uniform sampler2D emissive_sampler;

layout(binding = 9) uniform sampler2D ibl_lut_sampler;
layout(binding = 10) uniform samplerCubeArray ibl_diffuse_sampler;
layout(binding = 11) uniform samplerCubeArray ibl_specular_sampler;

in vertexOut {
  mat3 TBN;
  vec3 world_position; 
  vec2 texture_coordinate;
  vec3 normal;
} vertex_out;

layout(location = 0) out vec4 color;

void main()
{
    vec3 albedo = primitive_parameters.albedo.rgb;
    if (primitive_parameters.has_albedo_texture == 1) {
        albedo *= texture(albedo_sampler, vertex_out.texture_coordinate).rgb;
    }

    float metalness = primitive_parameters.metallic_roughness_refraction.r;
    float roughness = primitive_parameters.metallic_roughness_refraction.g;
    if (primitive_parameters.has_metallic_roughness_texture == 1) {
        metalness *= texture(metallic_roughness_sampler, vertex_out.texture_coordinate).b;
        roughness *= texture(metallic_roughness_sampler, vertex_out.texture_coordinate).g;
    }

    float occlusion = 1.0;
    if (primitive_parameters.has_occlusion_texture == 1) {
        occlusion = texture(occlusion_sampler, vertex_out.texture_coordinate).r;
    }

    vec3 normal = vertex_out.normal;
    if (primitive_parameters.has_normal_texture == 1) {
        normal = texture(normal_sampler, vertex_out.texture_coordinate).rgb;
        normal = normalize(normal * 2.0 - 1.0);
        normal = vertex_out.TBN * normal; 
    }
    normal = normalize(normal);

    vec3 emission = primitive_parameters.emission.rgb;
    if(primitive_parameters.has_emissive_texture == 1) {
        emission *= texture(emissive_sampler, vertex_out.texture_coordinate).rgb;
    }
    
    vec3 Lo = vec3(0.0);

    vec3 N = normal;
    vec3 V = normalize(vec3(camera.position) - vertex_out.world_position);
    vec3 R = reflect(-V, N); 

    // Approximation of F0 for Fresnel
    vec3 f0 = mix(vec3(0.04, 0.04, 0.04), albedo.rgb, metalness);

    // Image Based Lighting
    vec3 ambient = vec3(0.0, 0.0, 0.0);
    if (primitive_parameters.has_diffuse_ibl == 1) {
        const float MAX_REFLECTION_LOD = 7.0;

        vec2 brdf  = texture(ibl_lut_sampler, vec2(max(dot(N, V), 0.0), roughness)).rg;

        vec3 kS = fresnelSchlickRoughness(max(dot(N, V), 0.0), f0, roughness);
        vec3 kD = 1.0 - kS;  
        
        vec3 irradiance = texture(ibl_diffuse_sampler, vec4(N, 0)).rgb;
        vec3 diffuse = irradiance * albedo.rgb;
        kD *= 1.0 - metalness;
        
        // sample both the pre-filter map and the BRDF lut and combine them together as per the Split-Sum approximation to get the IBL specular part.
        vec3 prefiltered_color = textureLod(ibl_specular_sampler, vec4(R, 0), roughness * MAX_REFLECTION_LOD).rgb;    

        vec3 specular = prefiltered_color * (kS * brdf.x + brdf.y);

        ambient = (kD * diffuse + specular) * occlusion;
    } else {
        Lo = Lo * occlusion;
    }

    Lo = Lo + ambient;
    Lo = Lo + emission;

    // Post-process
    Lo = filmic_postprocess(Lo, 1.0);
    Lo = pow(Lo, vec3(1.0 / 2.2));

    color = vec4(Lo, 1.0);
}