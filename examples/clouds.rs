#![windows_subsystem = "windows"]

extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain! {}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;

quick_main!(run);

fn run() -> Result<()> {
    let mut app = AppBuilder::new()
        .with_title("Clouds")
        .with_dimensions(1280, 720)
        .build();

    let use_precomputed_noise = true;
    let mut reprojection_factor = 2;
    let mut max_iterations = 256;

    let sun_speed = 0.4;

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    app.renderer.set_disable(EnableOption::DepthTest);
    app.renderer.set_disable(EnableOption::CullFace);
    app.renderer.set_enable(EnableOption::Blend);
    app.renderer
        .set_blending_equation(BlendingEquation::Addition, BlendingEquation::Addition);
    app.renderer.set_linear_blending_factors(
        LinearBlendingFactor::SourceAlpha,
        LinearBlendingFactor::OneMinusSourceAlpha,
        LinearBlendingFactor::SourceAlpha,
        LinearBlendingFactor::OneMinusSourceAlpha,
    );

    let mut camera = SphereCamera::new(
        SpherePosition {
            radius: 1.0,
            y_angle: Rad(4.0583353),
            height_angle: Rad(2.0496094),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(45.0).into(),
        app.width,
        app.height,
        0.0001,
        100.0,
    )
    .chain_err(|| "Could not create camera")?;

    let mut input = Input::default();

    let mut y_angle = Rad(-1.75);
    let mut height_angle = Rad(-0.89).into();

    let atmosphere_module =
        AtmosphereModule::new(&mut app.renderer).chain_err(|| "Could not create atmosphere module")?;

    let raymarch_vertex = Shader::new(
        ShaderType::Vertex,
        &[include_str!("../assets/shaders/deferred_render.vert")],
    )
    .chain_err(|| "Failed to compile raymarch vertex shader")?;
    let raymarch_fragment = Shader::new(
        ShaderType::Fragment,
        &[
            include_str!("../assets/shaders/libs/version.glsl"),
            include_str!("../assets/shaders/libs/consts.glsl"),
            include_str!("../assets/shaders/atmosphere/structures.glsl"),
            include_str!("../assets/shaders/atmosphere/constants.glsl"),
            "#define MALLUMO_ATMOSPHERE_GLOBALS 1\n",
            include_str!("../assets/shaders/atmosphere/render.glsl"),
            include_str!("../assets/shaders/libs/filmic_tonemapping.glsl"),
            include_str!("../assets/shaders/clouds/render.frag"),
        ],
    )
    .chain_err(|| "Failed to compile raymarch fragment shader")?;

    let raymarch_pipeline = PipelineBuilder::new()
        .vertex_shader(&raymarch_vertex)
        .fragment_shader(&raymarch_fragment)
        .build()
        .chain_err(|| "Unable to build raymarch pipeline")?;

    let mut reprojection_module;
    let mut clouds_texture;
    let mut clouds_fbo;

    // no need for reprojection_tuple to pollute the scope
    {
        let reprojection_tuple = create_reprojection(app.width, app.height, reprojection_factor)
            .chain_err(|| "Could not create reprojection variables")?;

        reprojection_module = reprojection_tuple.0;
        clouds_texture = reprojection_tuple.1;
        clouds_fbo = reprojection_tuple.2;
    }

    let perlin_worley_noise = if use_precomputed_noise {
        Texture3D::new(
            Texture3DSize(128, 128, 128),
            TextureInternalFormat::RGBA8,
            TextureFormat::RGBA,
            TextureDataType::Byte,
            Texture3DParameters {
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                mipmap: TextureMipmapFilter::None,
                wrap_s: TextureWrapMode::Repeat,
                wrap_t: TextureWrapMode::Repeat,
                wrap_r: TextureWrapMode::Repeat,
            },
            1,
            include_bytes!("../assets/clouds/noise_128_128_128_4.bytes"),
        )
        .chain_err(|| "Could not create noise texture")?
    } else {
        create_pw1_w1_w2_w4_3d_noise(&mut app.renderer, 128).chain_err(|| "Could not create main noise texture")?
    };

    let worley_noise = if use_precomputed_noise {
        Texture3D::new(
            Texture3DSize(32, 32, 32),
            TextureInternalFormat::RGB8,
            TextureFormat::RGB,
            TextureDataType::Byte,
            Texture3DParameters {
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                mipmap: TextureMipmapFilter::None,
                wrap_s: TextureWrapMode::Repeat,
                wrap_t: TextureWrapMode::Repeat,
                wrap_r: TextureWrapMode::Repeat,
            },
            1,
            include_bytes!("../assets/clouds/noise_detail_32_32_32_3.bytes"),
        )
        .chain_err(|| "Could not create noise texture")?
    } else {
        create_w1_w2_w4_3d_noise(&mut app.renderer, 32).chain_err(|| "Could not create detail noise texture")?
    };

    let curl_noise = create_curl_2d_noise(&mut app.renderer, 128).chain_err(|| "Could not create curl noise")?;

    let coverage_texture = Texture2D::new(
        Texture2DSize(512, 512),
        TextureInternalFormat::RGB8,
        TextureFormat::RGB,
        TextureDataType::UnsignedByte,
        Texture2DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::Repeat,
            wrap_t: TextureWrapMode::Repeat,
        },
        1,
        include_bytes!("../assets/clouds/coverage_512_512_3.bytes"),
    )
    .chain_err(|| "Could not create coverage texture")?;

    let mut full_frame_fbo = GeneralFramebuffer::new();
    full_frame_fbo.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    full_frame_fbo.set_disable(EnableOption::DepthTest);
    full_frame_fbo.set_disable(EnableOption::CullFace);

    let draw_texture_vertex = Shader::new(
        ShaderType::Vertex,
        &[include_str!("../assets/shaders/deferred_render.vert")],
    )
    .chain_err(|| "Failed to compile draw texture vertex shader")?;

    let draw_texture_fragment = Shader::new(
        ShaderType::Fragment,
        &[
            include_str!("../assets/shaders/libs/version.glsl"),
            include_str!("../assets/shaders/libs/consts.glsl"),
            include_str!("../assets/shaders/atmosphere/structures.glsl"),
            include_str!("../assets/shaders/atmosphere/constants.glsl"),
            "#define MALLUMO_ATMOSPHERE_GLOBALS 1\n",
            include_str!("../assets/shaders/atmosphere/render.glsl"),
            include_str!("../assets/shaders/libs/filmic_tonemapping.glsl"),
            include_str!("../assets/shaders/clouds/mix_clouds_atmosphere.frag"),
        ],
    )
    .chain_err(|| "Failed to compile draw texture fragment shader")?;

    let draw_texture_pipeline = PipelineBuilder::new()
        .vertex_shader(&draw_texture_vertex)
        .fragment_shader(&draw_texture_fragment)
        .build()
        .chain_err(|| "Unable to build draw texture pipeline")?;

    let mut changing_viewport = false;

    let mut previous_time = time::precise_time_ns();
    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let delta_time = (current_time - previous_time) as f32 / 1_000_000_000.0;

        let sun_dir = [sun_to_dir(y_angle, height_angle)];
        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        {
            let dtt = DrawTextureTarget {
                color0: DrawTextureAttachOption::AttachTexture(&mut clouds_texture),
                ..Default::default()
            };
            let draw_command = DrawCommand::arrays(&raymarch_pipeline, 0, 3)
                .framebuffer(&clouds_fbo)
                .attachments(&dtt)
                .uniform(camera.get_buffer(), 0)
                // reprojection
                .uniform_2f(app.width as f32, app.height as f32, 1)
                .uniform_1f(reprojection_module.sub_pixel_size as f32, 2)
                .uniform_1f(reprojection_module.get_current_pixel() as f32, 3)
                // clouds
                .uniform_1f(max_iterations as f32, 4)
                .texture_3d(&perlin_worley_noise, 0)
                .texture_3d(&worley_noise, 1)
                .texture_2d(&curl_noise, 2)
                .texture_2d(&coverage_texture, 3)
                // atmosphere
                .uniform(&atmosphere_module.globals_buffer, 1)
                .uniform_3fv(&sun_dir, 0)
                .texture_2d(&atmosphere_module.transmittance_texture, 4)
                .texture_3d(&atmosphere_module.scattering_texture, 5)
                .texture_2d(&atmosphere_module.irradiance_texture, 6);

            app.renderer
                .draw(&draw_command)
                .chain_err(|| "Could not render clouds")?;
        }

        reprojection_module
            .reproject(&mut app.renderer, &clouds_texture, &camera)
            .chain_err(|| "Could not reproject")?;

        {
            let draw_command = DrawCommand::arrays(&draw_texture_pipeline, 0, 3)
                .uniform(camera.get_buffer(), 0)
                .texture_2d(&reprojection_module.accumulator, 0)
                // atmosphere
                .uniform(&atmosphere_module.globals_buffer, 1)
                .uniform_3fv(&sun_dir, 0)
                .texture_2d(&atmosphere_module.transmittance_texture, 1)
                .texture_3d(&atmosphere_module.scattering_texture, 2)
                .texture_2d(&atmosphere_module.irradiance_texture, 3);

            app.renderer
                .draw(&draw_command)
                .chain_err(|| "Could not draw texture")?;
        }

        app.swap_buffers();

        let mut change_reprojection = false;
        let mut changing_viewport_this_frame = false;
        for event in app.poll_events() {
            input.process_event(&event);
            camera
                .process_event(&event, &input, delta_time)
                .chain_err(|| "Could not process event in camera")?;

            match event {
                Event::WindowEvent {
                    event: glutin::WindowEvent::Resized(width, height),
                    ..
                } => {
                    if width != 0 && height != 0 {
                        changing_viewport_this_frame = true;

                        app.width = width as usize;
                        app.height = height as usize;
                        app.renderer.set_viewport(Viewport {
                            x: 0,
                            y: 0,
                            width: app.width,
                            height: app.height,
                        });
                        full_frame_fbo.set_viewport(Viewport {
                            x: 0,
                            y: 0,
                            width: width as usize,
                            height: height as usize,
                        });
                    }
                }
                Event::WindowEvent {
                    event: glutin::WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => {}
            }
        }

        if changing_viewport && !changing_viewport_this_frame {
            change_reprojection = true;
        }
        changing_viewport = changing_viewport_this_frame;

        // Sun
        let sun_delta = Rad(delta_time * sun_speed);
        if input.keyboard.D.is_pressed() {
            y_angle -= sun_delta;
        }
        if input.keyboard.A.is_pressed() {
            y_angle += sun_delta;
        }
        if input.keyboard.S.is_pressed() {
            height_angle -= sun_delta;
        }
        if input.keyboard.W.is_pressed() {
            height_angle += sun_delta;
        }

        // reprojection factor
        if input.keyboard.T.is_pressed() && reprojection_factor != 1 {
            reprojection_factor = 1;
            change_reprojection = true;
        }
        if input.keyboard.Y.is_pressed() && reprojection_factor != 2 {
            reprojection_factor = 2;
            change_reprojection = true;
        }
        if input.keyboard.U.is_pressed() && reprojection_factor != 4 {
            reprojection_factor = 4;
            change_reprojection = true;
        }
        if input.keyboard.I.is_pressed() && reprojection_factor != 8 {
            reprojection_factor = 8;
            change_reprojection = true;
        }

        // max iterations
        if input.keyboard.G.is_pressed() {
            max_iterations = 128;
        }
        if input.keyboard.H.is_pressed() {
            max_iterations = 256;
        }
        if input.keyboard.J.is_pressed() {
            max_iterations = 512;
        }
        if input.keyboard.K.is_pressed() {
            max_iterations = 1024;
        }

        if change_reprojection {
            let reprojection_tuple = create_reprojection(app.width, app.height, reprojection_factor)
                .chain_err(|| "Could not create reprojection variables")?;

            reprojection_module = reprojection_tuple.0;
            clouds_texture = reprojection_tuple.1;
            clouds_fbo = reprojection_tuple.2;
        }

        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        previous_time = current_time;
    }

    Ok(())
}

fn sun_to_dir(y_angle: cgmath::Rad<f32>, h: cgmath::Rad<f32>) -> [f32; 3] {
    let x = Rad::sin(h) * Rad::sin(y_angle);
    let y = Rad::cos(h);
    let z = Rad::sin(h) * Rad::cos(y_angle);

    [x, y, z]
}

fn create_reprojection(
    width: usize,
    height: usize,
    reprojection_factor: usize,
) -> Result<(ReprojectionModule, Texture2D, GeneralFramebuffer)> {
    let sub_width = std::cmp::max(width / reprojection_factor, 1);
    let sub_height = std::cmp::max(height / reprojection_factor, 1);
    let reprojection_module = ReprojectionModule::new(Texture2DSize(sub_width, sub_height), reprojection_factor, true)
        .chain_err(|| "Could not create reproject module")?;
    let clouds_texture = Texture2D::new_empty(
        Texture2DSize(sub_width, sub_height),
        TextureInternalFormat::RGBA32F,
        TextureFormat::RGBA,
        TextureDataType::Byte,
        Texture2DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
        },
        1,
    )
    .chain_err(|| "Could not create sub texture")?;
    let mut clouds_fbo = GeneralFramebuffer::new();
    clouds_fbo.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: sub_width,
        height: sub_height,
    });
    clouds_fbo.set_disable(EnableOption::DepthTest);
    clouds_fbo.set_disable(EnableOption::CullFace);

    Ok((reprojection_module, clouds_texture, clouds_fbo))
}
