extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;
extern crate structopt;

mod errors {
    error_chain! {}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;
use structopt::StructOpt;

quick_main!(run);

fn run() -> Result<()> {
    //
    // Process command line arguments
    //
    let mut arguments = VXGIArguments::from_args();

    if arguments.files.len() == 0 {
        arguments.files.push("Objects/poliform_bed/PoliformBed.obj".to_string());
    }

    // Initialize application (window, context, input, ui)
    let mut app = AppBuilder::new()
        .with_dimensions(1920, 1080) //
        .with_title("VXGI example")
        .build();
    let mut input = Input::default();

    let mut options = VXGIOptions::from_arguments(&arguments);
    options.set_levels(8);
    options.set_calculate_direct(false);
    options.set_calculate_indirect_diffuse(false);
    options.set_calculate_ambient_occlusion(true);
    options.set_cones(2, ConeDistribution::ConcentricRegular);

    let mut app_ui = VXGIOptionsUI::new(app.width, app.height, options);

    app.renderer.set_disable(EnableOption::DepthTest);
    app.renderer.set_disable(EnableOption::FramebufferSRGB);

    // Load scene
    let mut shape_list = ShapeList::from_files(
        arguments.files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: true,
        }),
    )
    .chain_err(|| "Could not create shape list")?;

    for parameters in &mut shape_list.primitive_parameters {
        if parameters.emission != Vector4::new(0.0, 0.0, 0.0, 0.0) {
            parameters.emission *= 100.0;
        }
    }

    shape_list
        .update_buffers()
        .chain_err(|| "Could not update main shape list buffers")?;

    //
    // Initialize modules for rendering
    //

    // - UI
    let mut ui_renderer = GuiRenderer::new(app.width, app.height, app.gl_window.hidpi_factor() as f64)
        .chain_err(|| "Unable to create GUI renderer")?;

    // - Camera
    let mut camera = SphereCamera::new(
        SpherePosition {
            radius: 3.349999, 
            y_angle: Rad(5.378981), 
            height_angle: Rad(1.0719601),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.0001,
        100.0,
    ).chain_err(|| "Could not create camera")?;
    camera.update_buffer().chain_err(|| "Could not update buffer")?;

    // - Sun
    let sun_module = SunModule::new().expect("Could not initialize Sun module");
    let mut sun = sun_module
        .create_sun(
            Vector3::new(1.0, 1.0, 1.0),
            SunPosition {
                y_angle: Rad(1.8877113),
                height_angle: Rad(1.129871),
            },
            512,
        )
        .expect("Could not initialize the Sun");

    sun.render_shadowmap_fast(&mut app.renderer, &mut [&mut shape_list])
        .chain_err(|| "Could not render shadowmap")?;

    // Deferred
    let mut deferred_collector =
        DefferedCollector::new(app.width, app.height).chain_err(|| "Could not create deferred collector")?;

    // - VXGI
    //   defaults to Texture Isotropic variant, may be changed to any VXGIModule (hence the trait)
    //   voxelize static part of geometry before render loop
    let voxel_structure = app_ui.gi_options.voxel_structure;
    let anisotropy = app_ui.gi_options.anisotropic();

    let mut vxgi_module: Box<VXGIModule> = match (voxel_structure, anisotropy) {
        (VoxelStructure::VolumeTexture, false) => {
            Box::new(IsotropicTextureGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
        }
        (VoxelStructure::VolumeTexture, true) => {
            Box::new(AnisotropicTextureGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
        }
        (VoxelStructure::SparseVoxelOctree, false) => {
            Box::new(IsotropicSVOGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
        }
        (VoxelStructure::SparseVoxelOctree, true) => {
            Box::new(AnisotropicSVOGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
        }
    };

    vxgi_module
        .voxelize(&mut app.renderer, &[&shape_list], true)
        .chain_err(|| "Could not voxelize static geometry")?;
    vxgi_module
        .inject_radiance(&mut app.renderer, &sun)
        .chain_err(|| "Could not inject radiance")?;

    let mut inject_direct = true;
    let mut third_bounce = true;

    let mut show_ui = false;
    let mut previous_time = time::precise_time_ns();
    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let current_time_ms = (current_time as f64) / (1_000_000f64);
        let current_time_s = current_time_ms / (1_000f64);
        let delta_time = (current_time - previous_time) as f32 / 1000000000.0;

        let cones = (current_time / 1_000_000_000u64) % 32 + 1;
        app_ui.gi_options.set_cones(cones as u32, ConeDistribution::ConcentricRegular);
        vxgi_module.update_options(app_ui.gi_options);

        println!("{} {:?}", cones, (current_time_s / 50f64) as f32);
        camera.set_y(Rad((current_time_s / 50f64) as f32));
        
        // Process the events
        for event in app.poll_events() {
            app_ui.process_event(event.clone(), &app.gl_window);

            match event.clone() {
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    if position.0 > 220.0 {
                        input.process_event(&event);
                        camera
                            .process_event(&event, &input, 0.0333)
                            .chain_err(|| "Could not process event in camera")?;
                    }
                }
                _ => {
                    input.process_event(&event);
                    camera
                        .process_event(&event, &input, 0.0333)
                        .chain_err(|| "Could not process event in camera")?;
                }
            };

            match event {
                Event::WindowEvent {
                    event: WindowEvent::Resized(w, h),
                    ..
                } => {
                    app.width = w as usize;
                    app.height = h as usize;

                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: w as usize,
                        height: h as usize,
                    });

                    deferred_collector
                        .render(&mut app.renderer, &[&shape_list], &camera)
                        .chain_err(|| "Could not render deferred textures")?;

                    ui_renderer
                        .resize(w as usize, h as usize)
                        .chain_err(|| "Could not resize GUI")?;
                }
                Event::WindowEvent {
                    event: WindowEvent::KeyboardInput { input, .. },
                    ..
                } => {
                    if input.state == ElementState::Pressed {
                        if let Some(keycode) = input.virtual_keycode {
                            match keycode {
                                glutin::VirtualKeyCode::D => {
                                    println!("{:?}", inject_direct);
                                    inject_direct = !inject_direct;
                                }
                                glutin::VirtualKeyCode::S => {
                                    third_bounce = !third_bounce;
                                }
                                glutin::VirtualKeyCode::H => {
                                    show_ui = !show_ui;
                                }
                                glutin::VirtualKeyCode::R => {
                                    vxgi_module.reload_shaders();
                                }
                                _ => {}
                            }
                        }
                    }
                }
                Event::WindowEvent {
                    event: WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => (),
            };
        }

        //
        // Update part of loop
        //
        // - Camera
        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        // - Shadowmap if sun's position was changed
        if app_ui.sun_variables.update {
            sun.set_position(SunPosition {
                y_angle: Rad(app_ui.sun_variables.angle_x),
                height_angle: Rad(app_ui.sun_variables.angle_y),
            })
            .chain_err(|| "Could not set Sun's poisition")?;

            app_ui.sun_variables.update = false;
        }

        if app_ui.update_gi_options {
            vxgi_module.update_options(app_ui.gi_options);
            app_ui.update_gi_options = false;
        }

        if app_ui.update_gi_structure {
            let voxel_structure = app_ui.gi_options.voxel_structure;
            let anisotropy = app_ui.gi_options.anisotropic();

            vxgi_module = match (voxel_structure, anisotropy) {
                (VoxelStructure::VolumeTexture, false) => {
                    Box::new(IsotropicTextureGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
                }
                (VoxelStructure::VolumeTexture, true) => {
                    Box::new(AnisotropicTextureGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
                }
                (VoxelStructure::SparseVoxelOctree, false) => {
                    Box::new(IsotropicSVOGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
                }
                (VoxelStructure::SparseVoxelOctree, true) => {
                    Box::new(AnisotropicSVOGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
                }
            };
            vxgi_module
                .voxelize(&mut app.renderer, &[&shape_list], true)
                .chain_err(|| "Could not voxelize static geometry")?;
            vxgi_module
                .inject_radiance(&mut app.renderer, &sun)
                .chain_err(|| "Could not inject radiance")?;
            app_ui.update_gi_structure = false;
        }

        // - UI
        app_ui.update_ui();

        //
        // Render part of the loop
        //

        // Clear default framebuffer
        app.renderer.clear_default_framebuffer(ClearBuffers::Color);

        app.renderer
            .set_blending_equation(BlendingEquation::Addition, BlendingEquation::Addition);
        app.renderer.set_linear_blending_factors(
            LinearBlendingFactor::SourceAlpha,
            LinearBlendingFactor::OneMinusSourceAlpha,
            LinearBlendingFactor::SourceAlpha,
            LinearBlendingFactor::OneMinusSourceAlpha,
        );

        app.renderer.set_enable(EnableOption::Multisample);
        deferred_collector
            .render_fast(&mut app.renderer, &mut [&mut shape_list], &camera)
            .chain_err(|| "Could not render deferred textures")?;

        //
        app.renderer.set_disable(EnableOption::Multisample);
        vxgi_module
            .render(&mut app.renderer, &deferred_collector, &camera, &sun)
            .chain_err(|| "Could not create finalrender")?;

        // Render UI as a last step
        if show_ui {
            ui_renderer
                .render(&mut app.renderer, &app_ui.get_ui(), &app_ui.get_image_map())
                .chain_err(|| "Could not render GUI")?;
        }

        app.swap_buffers();
    }

    Ok(())
}
