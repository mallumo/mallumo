extern crate cgmath;
extern crate glutin;
extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain! {}
}

use errors::*;

use cgmath::prelude::*;
use cgmath::{Deg, Point3, Rad, Vector3};
use mallumo::glutin::*;
use mallumo::*;

quick_main!(run);

fn run() -> Result<()> {
    let mut app = AppBuilder::new()
        .with_title("Scene example")
        .with_dimensions(1920, 1080)
        .with_multisampling(8)
        .build();
    let mut input = Input::default();

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });

    // Load scene
    let files = std::env::args().skip(1).map(|s| s.clone()).collect::<Vec<String>>();
    if files.len() == 0 {
        panic!("usage: gltf-display <PATH>");
    }

    let mut shape_list = ShapeList::from_files(
        files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: true,
        }),
    )
    .chain_err(|| "Could not create shape list")?;

    for shape in &mut shape_list.primitive_parameters {
        shape.has_diffuse_ibl = 1;
        shape.has_specular_ibl = 1;
    }

    shape_list
        .update_buffers()
        .chain_err(|| "Could not update shape list buffers")?;

    let mut camera = SphereCamera::new(
        SpherePosition {
            radius: 1.5,
            y_angle: Rad(0.0),
            height_angle: Deg(45.0).into(),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.0001,
        100.0,
    )
    .chain_err(|| "Could not create camera")?;

    let mut scene_renderer = SceneRenderer::new(&mut app.renderer).chain_err(|| "Could not create scene renderer")?;

    // IBL
    let mut skybox = Skybox::new(TextureCubemapSize(512, 512), 1).chain_err(|| "Could not create skybox")?;
    let mut atmosphere_module =
        AtmosphereModule::new(&mut app.renderer).chain_err(|| "Could not create atmosphere module")?;

    let mut ibl_module =
        IBLModule::new(&mut app.renderer, &skybox.skybox, 32, 128).chain_err(|| "Could not create IBL")?;

    let mut previous_time = time::precise_time_ns();
    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let delta_time = (current_time - previous_time) as f32 / 1000000000.0;

        //
        let current_time_s = current_time / 1000000000u64;
        let current_time_ms = current_time / 1000000u64 - current_time_s * 1000;
        let hour = (current_time_s % 24) as u32;
        let minute = map_range((0, 1000), (0, 60), current_time_ms) as u32;

        let sun_position = SunPosition::from_time_and_position(
            2006,
            7,
            1,
            hour,
            minute,
            0,
            Rad(0.0), /*Deg(48.0f64)*/
            Rad(0.0), /* Deg(18.0f64)*/
        );

        // println!("{},{},{},{},{},{},{}", hour, minute, sun_position.y_angle.0, sun_position.height_angle.0,  sun_position.to_cartesian()[0], sun_position.to_cartesian()[1], sun_position.to_cartesian()[2]);

        // Recalculate atmosphere and image based lighting maps
        atmosphere_module
            .render_skybox(
                &mut app.renderer,
                &mut skybox,
                Point3 { x: 0.0, y: 0.0, z: 0.0 },
                sun_position.to_cartesian().to_vec(),
            )
            .chain_err(|| "Could not render skybox")?;

        ibl_module
            .recalculate(&mut app.renderer, &skybox.skybox)
            .chain_err(|| "Could not create IBL")?;

        // Skybox
        app.renderer.set_disable(EnableOption::DepthTest);
        app.renderer.set_disable(EnableOption::CullFace);

        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);
        skybox.render(&mut app.renderer, &camera).unwrap();

        // Scene
        app.renderer.clear_default_framebuffer(ClearBuffers::Depth);
        app.renderer.set_enable(EnableOption::DepthTest);
        app.renderer.set_enable(EnableOption::CullFace);
        app.renderer.set_disable(EnableOption::Blend);
        app.renderer
            .set_blending_equation(BlendingEquation::Addition, BlendingEquation::Addition);
        app.renderer.set_linear_blending_factors(
            LinearBlendingFactor::One,
            LinearBlendingFactor::Zero,
            LinearBlendingFactor::One,
            LinearBlendingFactor::Zero,
        );
        app.renderer.set_enable(EnableOption::Multisample);

        scene_renderer
            .render(
                &mut app.renderer,
                &[&shape_list],
                &camera,
                Some((&ibl_module.diffuse, &ibl_module.specular)),
            )
            .chain_err(|| "Could not render the scene")?;

        app.swap_buffers();

        for event in app.poll_events() {
            input.process_event(&event);
            camera
                .process_event(&event, &input, delta_time)
                .chain_err(|| "Could not process event in camera")?;

            match event {
                Event::WindowEvent {
                    event: glutin::WindowEvent::Resized(width, height),
                    ..
                } => {
                    if width != 0 && height != 0 {
                        app.renderer.set_viewport(Viewport {
                            x: 0,
                            y: 0,
                            width: width as usize,
                            height: height as usize,
                        });
                    }
                }
                Event::WindowEvent {
                    event: glutin::WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => {}
            }
        }

        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        previous_time = current_time;
    }

    Ok(())
}

fn sun_to_dir(y_angle: cgmath::Rad<f32>, h: cgmath::Rad<f32>) -> [f32; 3] {
    let x = Rad::sin(h) * Rad::sin(y_angle);
    let y = Rad::cos(h);
    let z = Rad::sin(h) * Rad::cos(y_angle);

    [x, y, z]
}

fn map_range(from_range: (u64, u64), to_range: (u64, u64), s: u64) -> u64 {
    to_range.0 + (s - from_range.0) * (to_range.1 - to_range.0) / (from_range.1 - from_range.0)
}
