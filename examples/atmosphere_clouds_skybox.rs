extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain! {}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;

quick_main!(run);

fn run() -> Result<()> {
    let mut app = AppBuilder::new()
        .with_title("Atmosphere")
        .with_dimensions(1280, 720)
        .build();

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });

    let mut camera = SphereCamera::new(
        SpherePosition {
            radius: 1.5,
            y_angle: Rad(0.0),
            height_angle: Rad(1.85),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.0001,
        100.0,
    )
    .chain_err(|| "Could not create camera")?;

    let mut input = Input::default();

    let mut y_angle = Rad(0.0);
    let mut height_angle = Deg(-60.0).into();

    let sun_speed = 0.4;

    let mut atmosphere_module =
        AtmosphereModule::new(&mut app.renderer).chain_err(|| "Could not create atmosphere module")?;

    let mut skybox = Skybox::new(TextureCubemapSize(512, 512), 1).chain_err(|| "Could not create skybox")?;

    let mut ibl_module =
        IBLModule::new(&mut app.renderer, &skybox.skybox, 32, 128).chain_err(|| "Could not create IBL")?;

    app.swap_buffers();

    fn average(times: &[u64]) -> u64 {
        times.iter().sum::<u64>() / times.len() as u64
    }

    let mut timer_query = TimerQuery::new();
    let number_of_runs = 10;
    let mut runs = Vec::new();

    for _ in 0..number_of_runs {
        timer_query.begin();

        atmosphere_module
            .render_skybox(
                &mut app.renderer,
                &mut skybox,
                Point3 {
                    x: 0.5,
                    y: 2000.0,
                    z: 0.5,
                },
                sun_to_dir(y_angle, height_angle).into(),
            )
            .chain_err(|| "Could not render skybox")?;

        ibl_module
            .recalculate(&mut app.renderer, &skybox.skybox)
            .chain_err(|| "")?;

        runs.push(timer_query.end_ns());
    }
    println!("& {:.2} ", average(&runs) as f64 / 1_000_000f64);

    app.swap_buffers();
    let mut previous_time = time::precise_time_ns();
    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let delta_time = (current_time - previous_time) as f32 / 1_000_000_000.0;

        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        skybox.render(&mut app.renderer, &camera).unwrap();

        ibl_module
            .recalculate(&mut app.renderer, &skybox.skybox)
            .chain_err(|| "")?;

        app.swap_buffers();

        for event in app.poll_events() {
            match event.clone() {
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    if position.0 > 220.0 {
                        input.process_event(&event);
                        camera
                            .process_event(&event, &input, 0.0333)
                            .chain_err(|| "Could not process event in camera")?;
                    }
                }
                _ => {
                    input.process_event(&event);
                    camera
                        .process_event(&event, &input, 0.0333)
                        .chain_err(|| "Could not process event in camera")?;
                }
            };

            match event {
                Event::WindowEvent {
                    event: WindowEvent::Resized(width, height),
                    ..
                } => {
                    app.width = width as usize;
                    app.height = height as usize;
                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: app.width,
                        height: app.height,
                    });
                }
                Event::WindowEvent {
                    event: glutin::WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => {}
            }
        }

        let sun_delta = Rad(delta_time * sun_speed);
        if input.keyboard.D.is_pressed() {
            y_angle -= sun_delta;
        }
        if input.keyboard.A.is_pressed() {
            y_angle += sun_delta;
        }
        if input.keyboard.S.is_pressed() {
            height_angle -= sun_delta;
        }
        if input.keyboard.W.is_pressed() {
            height_angle += sun_delta;
        }

        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        previous_time = current_time;
    }

    Ok(())
}

fn sun_to_dir(y_angle: cgmath::Rad<f32>, h: cgmath::Rad<f32>) -> [f32; 3] {
    let x = Rad::sin(h) * Rad::sin(y_angle);
    let y = Rad::cos(h);
    let z = Rad::sin(h) * Rad::cos(y_angle);

    [x, y, z]
}
