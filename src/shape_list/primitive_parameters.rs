use cgmath::prelude::One;
use cgmath::{Matrix4, Vector3, Vector4};

use mallumo_gls::TextureHandle;
/// Object parameters.
///
/// This structure can also be used on GPU side.
#[repr(C)]
#[derive(Copy, Clone)]
pub struct PrimitiveParameters {
    pub model_matrix: Matrix4<f32>,

    pub albedo: Vector4<f32>,
    pub metallic_roughness_refraction: Vector4<f32>,
    pub emission: Vector4<f32>,

    pub has_albedo_texture: u32,
    pub has_metallic_roughness_texture: u32,
    pub has_occlusion_texture: u32,
    pub has_normal_texture: u32,
    pub has_emissive_texture: u32,

    pub has_diffuse_ibl: u32,
    pub has_specular_ibl: u32,

    pub casts_shadows: u32,
    pub receives_shadows: u32,

    sampler_padding: u32,

    pub albedo_texture_resident: TextureHandle,
    pub normal_texture_resident: TextureHandle,
    pub metallic_roughness_texture_resident: TextureHandle,
    pub emission_texture_resident: TextureHandle,

    padding: [u8; 68],
}

impl std::fmt::Debug for PrimitiveParameters {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "PrimitiveParameters {{
                model_matrix: {:?},
                albedo: {:?},
                metallic_roughness_refraction: {:?},
                emission: {:?},

                has_albedo_texture: {:?},
                has_metallic_roughness_texture: {:?},
                has_occlusion_texture: {:?},
                has_normal_texture: {:?},
                has_emissive_texture: {:?},

                has_diffuse_ibl: {:?},
                has_specular_ibl: {:?},

                casts_shadows: {:?},
                receives_shadows: {:?},

                albedo_texture_resident: {:?},
                normal_texture_resident: {:?},
                metallic_roughness_texture_resident: {:?},
                emission_texture_resident: {:?},     
            }}",
            self.model_matrix,
            self.albedo,
            self.metallic_roughness_refraction,
            self.emission,
            self.has_albedo_texture,
            self.has_metallic_roughness_texture,
            self.has_occlusion_texture,
            self.has_normal_texture,
            self.has_emissive_texture,
            self.has_diffuse_ibl,
            self.has_specular_ibl,
            self.casts_shadows,
            self.receives_shadows,
            self.albedo_texture_resident,
            self.normal_texture_resident,
            self.metallic_roughness_texture_resident,
            self.emission_texture_resident,
        )
    }
}

impl PrimitiveParameters {
    /// Creates new Primitive Parameters.
    pub fn new(
        model_matrix: Matrix4<f32>,
        albedo: Vector4<f32>,
        metallic: f32,
        roughness: f32,
        emission: Vector3<f32>,
        reffraction_index: f32,
        casts_shadows: bool,
        receives_shadows: bool,
        has_albedo_texture: bool,
        has_metallic_roughness_texture: bool,
        has_occlusion_texture: bool,
        has_normal_texture: bool,
        has_emissive_texture: bool,
        has_diffuse_ibl: bool,
        has_specular_ibl: bool,
    ) -> PrimitiveParameters {
        PrimitiveParameters {
            model_matrix: model_matrix,

            albedo: albedo,
            metallic_roughness_refraction: Vector4::new(metallic, roughness, reffraction_index, 0.0),
            emission: Vector4::new(emission[0], emission[1], emission[2], 0.0),

            has_albedo_texture: has_albedo_texture as u32,
            has_metallic_roughness_texture: has_metallic_roughness_texture as u32,
            has_occlusion_texture: has_occlusion_texture as u32,
            has_normal_texture: has_normal_texture as u32,
            has_emissive_texture: has_emissive_texture as u32,

            has_diffuse_ibl: has_diffuse_ibl as u32,
            has_specular_ibl: has_specular_ibl as u32,

            casts_shadows: casts_shadows as u32,
            receives_shadows: receives_shadows as u32,

            sampler_padding: 0,

            albedo_texture_resident: TextureHandle::empty(),
            normal_texture_resident: TextureHandle::empty(),
            metallic_roughness_texture_resident: TextureHandle::empty(),
            emission_texture_resident: TextureHandle::empty(),

            padding: [0u8; 68],
        }
    }
}

impl Default for PrimitiveParameters {
    fn default() -> PrimitiveParameters {
        PrimitiveParameters {
            model_matrix: Matrix4::one(),

            albedo: Vector4::new(1.0, 1.0, 1.0, 1.0),
            metallic_roughness_refraction: Vector4::new(0.0, 1.0, 0.0, 0.0),
            emission: Vector4::new(0.0, 0.0, 0.0, 0.0),

            has_albedo_texture: 0,
            has_metallic_roughness_texture: 0,
            has_occlusion_texture: 0,
            has_normal_texture: 0,
            has_emissive_texture: 0,

            has_diffuse_ibl: 0,
            has_specular_ibl: 0,

            casts_shadows: 1,
            receives_shadows: 1,

            sampler_padding: 0,

            albedo_texture_resident: TextureHandle::empty(),
            normal_texture_resident: TextureHandle::empty(),
            metallic_roughness_texture_resident: TextureHandle::empty(),
            emission_texture_resident: TextureHandle::empty(),

            padding: [0u8; 68],
        }
    }
}
