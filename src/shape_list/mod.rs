mod errors {
    error_chain! {
        errors {
        }
    }
}

#[allow(non_upper_case_globals)]
#[allow(non_snake_case)]
#[allow(dead_code)]
mod mikktspace;
pub use self::mikktspace::*;

#[allow(dead_code)]
mod vertex;
pub use self::vertex::*;

mod primitive_parameters;
pub use self::primitive_parameters::*;

use mallumo_gls::*;
use shape_list::errors::*;

use std::borrow::Borrow;
use std::f32;
use std::path::Path;

use cgmath::One;
use cgmath::{Matrix4, Point3, Vector3, Vector4};

use image;
use image::*;
use ordermap::OrderMap;

/// Describes one single shape.
///
/// Textures are indices into some collection storing the textures.
#[derive(Copy, Clone, Debug)]
pub struct Shape {
    pub indices: (usize, usize),
    pub vertices: (usize, usize),

    pub albedo: Option<usize>,
    pub metallic_roughness: Option<usize>,
    pub occlusion: Option<usize>,
    pub normal: Option<usize>,
    pub emissive: Option<usize>,
}

// May be thought of as a flat scene graph.
/// Describes multiple shapes. This structure contains data sufficient for drawing.
pub struct ShapeList {
    pub shapes: Vec<Shape>,

    pub indices: Vec<u32>,
    pub vertices: Vec<Vertex>,

    pub primitive_parameters: Vec<PrimitiveParameters>,
    pub textures: Vec<Texture2D>,

    pub indices_buffer: MutableBuffer,
    pub vertices_buffer: MutableBuffer,
    pub primitive_parameters_buffer: MutableBuffer,

    pub indirect_buffer: Option<MutableBuffer>,
}

/// This structure is used as unitization parameters. The box is axis aligned.
#[derive(Copy, Clone, Debug)]
pub struct Unitization {
    pub box_min: Point3<f32>,
    pub box_max: Point3<f32>,
    pub unitize_if_fits: bool,
}

trait ToImage {
    fn to_image<T: AsRef<Path>>(&self, filepath: T) -> image::DynamicImage;
}

impl<'a> ToImage for gltf::Texture<'a> {
    fn to_image<T: AsRef<Path>>(&self, filepath: T) -> image::DynamicImage {
        let img = self.source();
        let img = match img.source() {
            gltf::image::Source::View { .. } => panic!("No support for image in buffer."),
            gltf::image::Source::Uri { uri, mime_type } => {
                if uri.starts_with("data:") {
                    panic!("No support for image in buffer.")
                } else if let Some(mime_type) = mime_type {
                    let path = filepath
                        .as_ref()
                        .parent()
                        .unwrap_or_else(|| std::path::Path::new("./"))
                        .join(uri);
                    let file = std::fs::File::open(path).unwrap();
                    let reader = std::io::BufReader::new(file);
                    match mime_type {
                        "image/jpeg" => image::load(reader, image::ImageFormat::JPEG),
                        "image/png" => image::load(reader, image::ImageFormat::PNG),
                        _ => panic!(format!(
                            "unsupported image type (image: {}, mime_type: {})",
                            img.index(),
                            mime_type
                        )),
                    }
                } else {
                    let path = filepath
                        .as_ref()
                        .parent()
                        .unwrap_or_else(|| std::path::Path::new("./"))
                        .join(uri);
                    image::open(path)
                }
            }
        };

        img.expect("Image loading failed.")
    }
}

fn flatten_nodes(
    node: &gltf::Node,
    parent_model: cgmath::Matrix4<f32>,
    meshes: &mut std::collections::HashMap<usize, cgmath::Matrix4<f32>>,
) {
    let model_matrix = parent_model * cgmath::Matrix4::<f32>::from(node.transform().matrix());

    if let Some(mesh) = node.mesh() {
        meshes.insert(mesh.index(), model_matrix);
    }

    for child in node.children() {
        if let Some(mesh) = child.mesh() {
            meshes.insert(mesh.index(), model_matrix);
        }

        flatten_nodes(&child, model_matrix, meshes);
    }
}

fn generate_tangents_deindexed(vertices: &mut Vec<Vertex>) -> Result<()> {
    unsafe {
        let mut mikkt_interface = SMikkTSpaceInterface {
            get_faces_count: mikkt_get_num_faces,
            get_vertices_count: mikkt_get_num_vertices_of_face,
            get_position: mikkt_get_position,
            get_normal: mikkt_get_normal,
            get_texture_coordinate: mikkt_get_texcoord,
            set_tspace_basic: mikkt_set_tspace_basic,
            set_tspace: mikkt_set_tspace,
        };

        let mikkt_context = SMikkTSpaceContext {
            interface: &mut mikkt_interface,
            data: vertices as *mut Vec<Vertex> as *mut ::std::os::raw::c_void,
        };

        let res: i32 = genTangSpaceDefault(&mikkt_context);

        if res != 1 {
            bail!("Could not calculate tangent");
        } else {
            Ok(())
        }
    }
}

fn parse_float3(numbers: &str) -> Vec<f32> {
    numbers.split_whitespace().map(|s| s.parse().unwrap()).collect()
}

impl ShapeList {
    pub fn new() -> Result<ShapeList> {
        let vertices_buffer =
            MutableBuffer::new_empty(std::mem::size_of::<Vertex>()).chain_err(|| "Could not create vertices buffer")?;
        let indices_buffer =
            MutableBuffer::new_empty(std::mem::size_of::<u32>()).chain_err(|| "Could not create indices buffer")?;
        let primitive_parameters_buffer = MutableBuffer::new_empty(std::mem::size_of::<PrimitiveParameters>())
            .chain_err(|| "Could not create primitive parameters buffer")?;

        Ok(ShapeList {
            shapes: Vec::new(),

            indices: Vec::new(),
            vertices: Vec::new(),

            primitive_parameters: Vec::new(),
            textures: Vec::new(),

            indices_buffer,
            vertices_buffer,
            primitive_parameters_buffer,

            indirect_buffer: None,
        })
    }

    pub fn push(&mut self, mut other: ShapeList) -> Result<()> {
        let previous_indices_len = self.indices.len();
        let previous_vertices_len = self.vertices.len();
        let previous_textures_len = self.textures.len();

        for shape in other.shapes {
            self.shapes.push(Shape {
                indices: (shape.indices.0 + previous_indices_len, shape.indices.1),
                vertices: (shape.vertices.0 + previous_vertices_len, shape.vertices.1),

                albedo: shape.albedo.and_then(|t| Some(t + previous_textures_len)),
                metallic_roughness: shape.metallic_roughness.and_then(|t| Some(t + previous_textures_len)),
                occlusion: shape.occlusion.and_then(|t| Some(t + previous_textures_len)),
                normal: shape.normal.and_then(|t| Some(t + previous_textures_len)),
                emissive: shape.emissive.and_then(|t| Some(t + previous_textures_len)),
            });
        }

        self.indices.append(&mut other.indices);
        self.vertices.append(&mut other.vertices);
        self.primitive_parameters.append(&mut other.primitive_parameters);
        self.textures.append(&mut other.textures);

        self.vertices_buffer = MutableBuffer::new(&self.indices).chain_err(|| "Could not create vertices buffer")?;
        self.indices_buffer = MutableBuffer::new(&self.vertices).chain_err(|| "Could not create indices buffer")?;
        self.primitive_parameters_buffer = MutableBuffer::new(&self.primitive_parameters)
            .chain_err(|| "Could not create primitive parameters buffer")?;

        Ok(())
    }

    pub fn append(&mut self, mut others: Vec<ShapeList>) -> Result<()> {
        for shape_list in others.drain(0..) {
            self.push(shape_list).chain_err(|| "Could not add shape list")?;
        }

        Ok(())
    }

    /// Creates Shape List from file.
    ///
    /// Currently only .obj and .gltf files are supported.
    pub fn from_file<T: AsRef<Path>>(filepath: T, unitization: Option<Unitization>) -> Result<ShapeList> {
        let has_supported_extensions =
            supported_file_format(filepath.as_ref()).chain_err(|| "Could not determine if file is supported")?;
        if !has_supported_extensions {
            bail!("File format is not supported");
        }

        let extension = filepath.as_ref().extension().unwrap().to_str().unwrap();
        match extension {
            "obj" => ShapeList::from_obj(filepath.as_ref(), unitization),
            "gltf" => ShapeList::from_gltf(filepath.as_ref(), unitization),
            _ => unreachable!(),
        }
    }

    /// Create Shape List from multiple files.
    ///
    /// Currently only .obj and .gltf files are supported.
    pub fn from_files<T: AsRef<Path>>(filepaths: &[T], unitization: Option<Unitization>) -> Result<ShapeList> {
        if filepaths.len() == 0 {
            bail!("At least one path must be provided");
        }

        let mut shape_lists = Vec::new();

        for filepath in filepaths {
            shape_lists.push(ShapeList::from_file(filepath, unitization).chain_err(|| "")?);
        }

        let mut shape_list = shape_lists.remove(0);
        shape_list
            .append(shape_lists)
            .chain_err(|| "Could not append shape list")?;

        Ok(shape_list)
    }

    pub fn from_gltf<T: AsRef<Path>>(filepath: T, unitization: Option<Unitization>) -> Result<ShapeList> {
        let gltf = gltf::Gltf::open(filepath.as_ref()).chain_err(|| "Could not open glTF")?;
        let (_, buffers, _) = gltf::import(filepath.as_ref()).chain_err(|| "Could not import glTF")?;

        let mut shapes = Vec::new();

        let mut indices = Vec::new();
        let mut vertices = Vec::new();

        let mut primitive_parameters = Vec::new();

        // json index => Texture2D
        let mut textures_map: OrderMap<usize, Texture2D> = OrderMap::new();

        // Flatten Hierarchy
        let mut nodes = std::collections::HashMap::new();
        for scene in gltf.scenes() {
            for node in scene.nodes() {
                flatten_nodes(&node, cgmath::Matrix4::one(), &mut nodes);
            }
        }

        for node in gltf.nodes() {
            if let Some(mesh) = node.mesh() {
                let model_matrix = nodes[&mesh.index()];

                for primitive in mesh.primitives() {
                    let reader = primitive.reader(|buffer| Some(&buffers[buffer.index()]));

                    let prim_positions: Vec<[f32; 3]> = reader.read_positions().unwrap().collect();
                    let prim_indices: Vec<u32> = reader.read_indices().unwrap().into_u32().collect();
                    let prim_normals: Vec<[f32; 3]> = reader.read_normals().unwrap().collect();
                    let prim_tex_coords: Option<Vec<[f32; 2]>> = reader
                        .read_tex_coords(0)
                        .and_then(|tex_coords| Some(tex_coords.into_f32().collect()));

                    let aos_vertices: Vec<Vertex> = if let Some(tex_coords) = prim_tex_coords {
                        Vertex::soa_to_aos(
                            prim_positions.as_slice(),
                            prim_normals.as_slice(),
                            Some(tex_coords.as_slice()),
                            None,
                        )
                        .chain_err(|| "Could not convert SoA to AoS")?
                    } else {
                        Vertex::soa_to_aos(prim_positions.as_slice(), prim_normals.as_slice(), None, None)
                            .chain_err(|| "Could not convert SoA to AoS")?
                    };

                    // Generate tangents and binormals
                    let mut deindexed_vertices = deindex_vertices(&prim_indices, &aos_vertices);
                    {
                        generate_tangents_deindexed(&mut deindexed_vertices)
                            .chain_err(|| "Could not generate tangents")?;
                    }
                    let (mut indexed_indices, mut indexed_vertices) = index_vertices(&deindexed_vertices);

                    // Align positions and indices
                    let aligned_indices_len = if indexed_indices.len() % 64 == 0 {
                        indexed_indices.len()
                    } else {
                        ((indexed_indices.len() / 64) + 1) * 64
                    };

                    let aligned_vertices_len = if indexed_vertices.len() % 4 == 0 {
                        indexed_vertices.len()
                    } else {
                        ((indexed_vertices.len() / 4) + 1) * 4
                    };

                    let mut aligned_indices: Vec<u32> = indexed_indices.to_vec();
                    let mut aligned_vertices: Vec<Vertex> = indexed_vertices.to_vec();

                    for _ in 0..aligned_indices_len - indexed_indices.len() {
                        aligned_indices.push(0);
                    }

                    for _ in 0..aligned_vertices_len - indexed_vertices.len() {
                        aligned_vertices.push(Vertex::new(
                            [0.0f32, 0.0, 0.0],
                            [0.0f32, 0.0, 0.0],
                            [0.0f32, 0.0],
                            [0.0f32, 0.0, 0.0, 0.0],
                        ));
                    }

                    // Create materials and textures
                    let albedo = primitive.material().pbr_metallic_roughness().base_color_factor();
                    let metallic = primitive.material().pbr_metallic_roughness().metallic_factor();
                    let roughness = primitive.material().pbr_metallic_roughness().roughness_factor();
                    let emission = primitive.material().emissive_factor();

                    let mut parameters = PrimitiveParameters::default();
                    parameters.model_matrix = cgmath::Matrix4::from(model_matrix);
                    parameters.albedo = cgmath::Vector4::from(albedo);
                    parameters.metallic_roughness_refraction = cgmath::Vector4::new(metallic, roughness, 1.0, 0.0);
                    parameters.emission = cgmath::Vector4::new(emission[0], emission[1], emission[2], 1.0);

                    let albedo_texture_index = primitive
                        .material()
                        .pbr_metallic_roughness()
                        .base_color_texture()
                        .and_then(|info| {
                            let texture = info.texture();
                            parameters.has_albedo_texture = 1;

                            if textures_map.contains_key(&texture.index()) {
                                let (index, _, gl_texture) = textures_map.get_full(&texture.index()).unwrap();
                                parameters.albedo_texture_resident = gl_texture.get_handle();
                                Some(index)
                            } else {
                                let image = texture.to_image(&filepath).to_rgba();

                                let mut gl_texture = Texture2D::new(
                                    Texture2DSize(image.dimensions().0 as usize, image.dimensions().1 as usize),
                                    TextureInternalFormat::CompressedRGBADXT1,
                                    TextureFormat::RGBA,
                                    TextureDataType::UnsignedByte,
                                    Texture2DParameters {
                                        min: TextureTexelFilter::Linear,
                                        mag: TextureTexelFilter::Linear,
                                        mipmap: TextureMipmapFilter::Linear,
                                        ..Default::default()
                                    },
                                    MipmapLevel::Max,
                                    &image.into_raw(),
                                )
                                .unwrap();
                                gl_texture.generate_mipmap();

                                parameters.albedo_texture_resident = gl_texture.get_handle();
                                textures_map.insert(texture.index(), gl_texture);

                                Some(textures_map.len() - 1)
                            }
                        });

                    let metallic_roughness_texture_index = primitive
                        .material()
                        .pbr_metallic_roughness()
                        .metallic_roughness_texture()
                        .and_then(|info| {
                            let texture = info.texture();
                            parameters.has_metallic_roughness_texture = 1;

                            if textures_map.contains_key(&texture.index()) {
                                let (index, _, gl_texture) = textures_map.get_full(&texture.index()).unwrap();
                                parameters.metallic_roughness_texture_resident = gl_texture.get_handle();
                                Some(index)
                            } else {
                                let image = texture.to_image(&filepath).to_rgb();

                                let mut gl_texture = Texture2D::new(
                                    Texture2DSize(image.dimensions().0 as usize, image.dimensions().1 as usize),
                                    TextureInternalFormat::CompressedRGBDTX1,
                                    TextureFormat::RGB,
                                    TextureDataType::UnsignedByte,
                                    Texture2DParameters {
                                        min: TextureTexelFilter::Linear,
                                        mag: TextureTexelFilter::Linear,
                                        mipmap: TextureMipmapFilter::Linear,
                                        ..Default::default()
                                    },
                                    MipmapLevel::Max,
                                    &image.into_raw(),
                                )
                                .expect("Could not create metallic roughness texture");
                                gl_texture.generate_mipmap();

                                parameters.metallic_roughness_texture_resident = gl_texture.get_handle();
                                textures_map.insert(texture.index(), gl_texture);

                                Some(textures_map.len() - 1)
                            }
                        });

                    let normal_texture_index = primitive.material().normal_texture().and_then(|info| {
                        let texture = info.texture();
                        parameters.has_normal_texture = 1;

                        if textures_map.contains_key(&texture.index()) {
                            let (index, _, gl_texture) = textures_map.get_full(&texture.index()).unwrap();
                            parameters.normal_texture_resident = gl_texture.get_handle();
                            Some(index)
                        } else {
                            let image = texture.to_image(&filepath).to_rgb();

                            let mut gl_texture = Texture2D::new(
                                Texture2DSize(image.dimensions().0 as usize, image.dimensions().1 as usize),
                                TextureInternalFormat::CompressedRGBDTX1,
                                TextureFormat::RGB,
                                TextureDataType::UnsignedByte,
                                Texture2DParameters {
                                    min: TextureTexelFilter::Linear,
                                    mag: TextureTexelFilter::Linear,
                                    mipmap: TextureMipmapFilter::Linear,
                                    ..Default::default()
                                },
                                MipmapLevel::Max,
                                &image.into_raw(),
                            )
                            .expect("Could not create normal texture");
                            gl_texture.generate_mipmap();

                            parameters.normal_texture_resident = gl_texture.get_handle();
                            textures_map.insert(texture.index(), gl_texture);

                            Some(textures_map.len() - 1)
                        }
                    });

                    let emissive_texture_index = primitive.material().emissive_texture().and_then(|info| {
                        let texture = info.texture();
                        parameters.has_emissive_texture = 1;

                        if textures_map.contains_key(&texture.index()) {
                            let (index, _, gl_texture) = textures_map.get_full(&texture.index()).unwrap();
                            parameters.emission_texture_resident = gl_texture.get_handle();
                            Some(index)
                        } else {
                            let image = texture.to_image(&filepath).to_rgb();

                            let mut gl_texture = Texture2D::new(
                                Texture2DSize(image.dimensions().0 as usize, image.dimensions().1 as usize),
                                TextureInternalFormat::CompressedRGBDTX1,
                                TextureFormat::RGB,
                                TextureDataType::UnsignedByte,
                                Texture2DParameters {
                                    min: TextureTexelFilter::Linear,
                                    mag: TextureTexelFilter::Linear,
                                    mipmap: TextureMipmapFilter::Linear,
                                    ..Default::default()
                                },
                                MipmapLevel::Max,
                                &image.into_raw(),
                            )
                            .expect("Could not create emissive texture");
                            gl_texture.generate_mipmap();

                            parameters.emission_texture_resident = gl_texture.get_handle();
                            textures_map.insert(texture.index(), gl_texture);

                            Some(textures_map.len() - 1)
                        }
                    });

                    shapes.push(Shape {
                        indices: (indices.len() as usize, indexed_indices.len()),
                        vertices: (vertices.len() as usize, indexed_vertices.len()),

                        albedo: albedo_texture_index,
                        metallic_roughness: metallic_roughness_texture_index,
                        occlusion: None,
                        normal: normal_texture_index,
                        emissive: emissive_texture_index,
                    });
                    indices.append(&mut aligned_indices);
                    vertices.append(&mut aligned_vertices);
                    primitive_parameters.push(parameters);
                }
            }
        }

        if let Some(unitization) = unitization {
            unitize_model(
                &indices,
                &vertices,
                &mut primitive_parameters,
                &mut shapes,
                unitization.box_min,
                unitization.box_max,
                unitization.unitize_if_fits,
            );
        }

        let indices_buffer = MutableBuffer::new(&indices).chain_err(|| "Could not create indices buffer")?;
        let vertices_buffer = MutableBuffer::new(&vertices).chain_err(|| "Could not create vertices buffer")?;
        let primitive_parameters_buffer =
            MutableBuffer::new(&primitive_parameters).chain_err(|| "Could not create primitive parameters buffer")?;

        let textures = textures_map.into_iter().map(|(_, val)| val as Texture2D).collect();

        Ok(ShapeList {
            shapes,

            indices,
            vertices,

            primitive_parameters,
            textures,

            indices_buffer,
            vertices_buffer,
            primitive_parameters_buffer,

            indirect_buffer: None,
        })
    }

    /// Loads .obj file and returns ShapeList.
    pub fn from_obj<T: AsRef<Path>>(filepath: T, unitization: Option<Unitization>) -> Result<ShapeList> {
        let (models, materials) = match tobj::load_obj(filepath.as_ref()) {
            Ok((models, materials)) => (models, materials),
            Err(e) => bail!("Loading of {:?} failed due to {:?}", filepath.as_ref(), e),
        };

        let mut joined_indices = Vec::new();
        let mut joined_vertices = Vec::new();
        let mut primitive_parameters = Vec::new();
        let mut shapes = Vec::new();

        let mut textures: OrderMap<std::path::PathBuf, Texture2D> = OrderMap::new();

        let mut min_pos = [f32::INFINITY; 3];
        let mut max_pos = [f32::NEG_INFINITY; 3];

        let mut positions: Vec<[f32; 3]> = Vec::new();
        let mut normals: Vec<[f32; 3]> = Vec::new();
        let mut texcoords: Vec<[f32; 2]> = Vec::new();

        for model in models {
            let mesh = model.mesh;

            if mesh.positions.len() == 0 {
                bail!("Object does not have positions");
            }

            positions.clear();
            normals.clear();
            texcoords.clear();

            for i in 0..(mesh.positions.len() / 3) {
                let position = [
                    mesh.positions[i * 3],
                    mesh.positions[i * 3 + 1],
                    mesh.positions[i * 3 + 2],
                ];

                positions.push(position);

                for i in 0..3 {
                    min_pos[i] = f32::min(min_pos[i], position[i]);
                    max_pos[i] = f32::max(max_pos[i], position[i]);
                }
            }

            if mesh.normals.len() > 0 {
                for i in 0..(mesh.positions.len() / 3) {
                    normals.push([mesh.normals[i * 3], mesh.normals[i * 3 + 1], mesh.normals[i * 3 + 2]]);
                }
            } else {
                for _ in 0..positions.len() {
                    normals.push([0.0, 0.0, 1.0]);
                }
            }

            if mesh.texcoords.len() > 0 {
                for i in 0..(mesh.texcoords.len() / 2) {
                    texcoords.push([mesh.texcoords[i * 2], mesh.texcoords[i * 2 + 1]]);
                }
            } else {
                for _ in 0..positions.len() {
                    texcoords.push([0.0, 0.0]);
                }
            }

            let vertices = Vertex::soa_to_aos(
                positions.as_slice(),
                normals.as_slice(),
                if texcoords.is_empty() {
                    None
                } else {
                    Some(texcoords.as_slice())
                },
                None,
            )
            .chain_err(|| "Could not convert SoA to AoS")?;

            let mut deindexed_vertices = deindex_vertices(&mesh.indices, &vertices);

            generate_tangents_deindexed(&mut deindexed_vertices).chain_err(|| "Could not generate tangents")?;

            let (indices, vertices) = index_vertices(&deindexed_vertices);

            let mut albedo = Vector4::new(1.0, 1.0, 1.0, 1.0);
            let mut emission = Vector3::new(0.0, 0.0, 0.0);
            let mut has_albedo = false;
            let mut albedo_texture_handle = TextureHandle::empty();

            match mesh.material_id {
                Some(i) => {
                    albedo = Vector4::new(
                        materials[i].ambient[0],
                        materials[i].ambient[1],
                        materials[i].ambient[2],
                        1.0,
                    );

                    if let Some(string) = materials[i].unknown_param.get("Ke") {
                        let emission_vec = parse_float3(string.as_str());
                        emission[0] = emission_vec[0];
                        emission[1] = emission_vec[1];
                        emission[2] = emission_vec[2];
                    }

                    let albedo_image_obj_raw_path = &materials[i].diffuse_texture;
                    if !albedo_image_obj_raw_path.is_empty() {
                        let albedo_image_obj_path = Path::new(albedo_image_obj_raw_path);
                        let albedo_image_path = if albedo_image_obj_path.is_absolute() {
                            albedo_image_obj_path.to_path_buf()
                        } else {
                            match filepath.as_ref().parent() {
                                Some(parent) => parent.join(albedo_image_obj_path),
                                None => Path::new("/").join(albedo_image_obj_path),
                            }
                        };

                        if textures.contains_key(&albedo_image_path) {
                            has_albedo = true;
                            albedo_texture_handle = textures.get(&albedo_image_path).unwrap().get_handle();
                        } else {
                            match image::open(&albedo_image_path) {
                                Ok(image) => {
                                    let w;
                                    let h;
                                    let mut raw_rgba;

                                    match image {
                                        DynamicImage::ImageRgb8(ref image) => {
                                            w = image.width();
                                            h = image.height();
                                            let image_pixels = image.clone().into_raw();
                                            raw_rgba = Vec::with_capacity(w as usize * h as usize * 4);
                                            for i in 0..image_pixels.len() / 3 {
                                                raw_rgba.push(image_pixels[i * 3]);
                                                raw_rgba.push(image_pixels[i * 3 + 1]);
                                                raw_rgba.push(image_pixels[i * 3 + 2]);
                                                raw_rgba.push(std::u8::MAX);
                                            }
                                        }
                                        DynamicImage::ImageRgba8(ref image) => {
                                            w = image.width();
                                            h = image.height();
                                            raw_rgba = image.clone().into_raw();
                                        }
                                        DynamicImage::ImageLuma8(ref image) => {
                                            w = image.width();
                                            h = image.height();
                                            let image_pixels = image.clone().into_raw();
                                            raw_rgba = Vec::with_capacity(w as usize * h as usize * 4);
                                            for i in 0..image_pixels.len() {
                                                raw_rgba.push(image_pixels[i]);
                                                raw_rgba.push(image_pixels[i]);
                                                raw_rgba.push(image_pixels[i]);
                                                raw_rgba.push(std::u8::MAX);
                                            }
                                        }
                                        DynamicImage::ImageLumaA8(ref image) => {
                                            w = image.width();
                                            h = image.height();
                                            let image_pixels = image.clone().into_raw();
                                            raw_rgba = Vec::with_capacity(w as usize * h as usize * 4);
                                            for i in 0..image_pixels.len() / 2 {
                                                raw_rgba.push(image_pixels[i * 2]);
                                                raw_rgba.push(image_pixels[i * 2]);
                                                raw_rgba.push(image_pixels[i * 2]);
                                                raw_rgba.push(image_pixels[i * 2 + 1]);
                                            }
                                        }
                                    }

                                    let mut gl_texture = Texture2D::new(
                                        Texture2DSize(w as usize, h as usize),
                                        TextureInternalFormat::CompressedRGBADXT1,
                                        TextureFormat::RGBA,
                                        TextureDataType::UnsignedByte,
                                        Texture2DParameters {
                                            min: TextureTexelFilter::Linear,
                                            mag: TextureTexelFilter::Linear,
                                            mipmap: TextureMipmapFilter::None,
                                            ..Default::default()
                                        },
                                        MipmapLevel::Max,
                                        &raw_rgba,
                                    )
                                    .chain_err(|| "Could not create texture")?;
                                    gl_texture.generate_mipmap();

                                    has_albedo = true;
                                    albedo_texture_handle = gl_texture.get_handle();

                                    textures.insert(albedo_image_path, gl_texture);
                                }
                                Err(_) => {}
                            };
                        }
                    }
                }
                None => {}
            }

            let mut primitive_parameter = PrimitiveParameters::new(
                Matrix4::one(),
                albedo,
                0.0,
                1.0,
                emission,
                0.0,
                // Shadows are on by default
                true,
                true,
                has_albedo,
                false,
                false,
                false,
                false,
                false,
                false,
            );

            if has_albedo {
                let texture_index = textures.len() - 1;
                shapes.push(Shape {
                    indices: (joined_indices.len() as usize, indices.len()),
                    vertices: (joined_vertices.len() as usize, vertices.len()),

                    albedo: Some(texture_index),
                    metallic_roughness: None,
                    occlusion: None,
                    normal: None,
                    emissive: None,
                });

                primitive_parameter.albedo_texture_resident = albedo_texture_handle;
            } else {
                shapes.push(Shape {
                    indices: (joined_indices.len() as usize, indices.len()),
                    vertices: (joined_vertices.len() as usize, vertices.len()),

                    albedo: None,
                    metallic_roughness: None,
                    occlusion: None,
                    normal: None,
                    emissive: None,
                });
            }

            let aligned_indices_len = if indices.len() % 64 == 0 {
                indices.len()
            } else {
                ((indices.len() / 64) + 1) * 64
            };

            let aligned_vertices_len = if vertices.len() % 4 == 0 {
                vertices.len()
            } else {
                ((vertices.len() / 4) + 1) * 4
            };

            let mut aligned_indices = indices.clone();
            let mut aligned_vertices = vertices.clone();

            for _ in 0..aligned_indices_len - indices.len() {
                aligned_indices.push(0);
            }

            for _ in 0..aligned_vertices_len - vertices.len() {
                aligned_vertices.push(Vertex::new(
                    [0.0f32, 0.0, 0.0],
                    [0.0f32, 0.0, 0.0],
                    [0.0f32, 0.0],
                    [0.0f32, 0.0, 0.0, 0.0],
                ));
            }

            joined_indices.append(&mut aligned_indices);
            joined_vertices.append(&mut aligned_vertices);
            primitive_parameters.push(primitive_parameter);
        }

        let textures = textures.into_iter().map(|(_, val)| val as Texture2D).collect();

        if let Some(unitization) = unitization {
            unitize_model(
                &joined_indices,
                &joined_vertices,
                &mut primitive_parameters,
                &mut shapes,
                unitization.box_min,
                unitization.box_max,
                unitization.unitize_if_fits,
            );
        }

        let indices_buffer = MutableBuffer::new(&joined_indices).chain_err(|| "Could not create indices buffer")?;
        let vertices_buffer = MutableBuffer::new(&joined_vertices).chain_err(|| "Could not create vertices buffer")?;
        let primitive_parameters_buffer =
            MutableBuffer::new(&primitive_parameters).chain_err(|| "Could not create primitive parameters buffer")?;

        Ok(ShapeList {
            shapes,

            indices: joined_indices,
            vertices: joined_vertices,

            primitive_parameters,
            textures,

            indices_buffer,
            vertices_buffer,
            primitive_parameters_buffer,

            indirect_buffer: None,
        })
    }

    /// Updates Shape List's buffer, necessary to use after any changes(if changes are supposed to be seen on GPU side).
    pub fn update_buffers(&mut self) -> Result<()> {
        self.indices_buffer =
            MutableBuffer::new(self.indices.as_slice()).chain_err(|| "Could not create new indices buffer")?;
        self.vertices_buffer =
            MutableBuffer::new(self.vertices.as_slice()).chain_err(|| "Could not create new vertices buffer")?;
        self.primitive_parameters_buffer = MutableBuffer::new(self.primitive_parameters.as_slice())
            .chain_err(|| "Could not create new primitive parameters buffer")?;

        Ok(())
    }

    /// Adds shape into Shape List.
    pub fn add_shape(
        &mut self,
        indices: &[u32],
        vertices: &[Vertex],
        primitive_parameters: &mut [PrimitiveParameters],
        _albedo: Option<Texture2D>,
        _metallic_roughness: Option<Texture2D>,
        _occlusion: Option<Texture2D>,
        _normal: Option<Texture2D>,
        _emissive: Option<Texture2D>,
        unitization: Option<Unitization>,
    ) -> Result<()> {
        let shape = Shape {
            indices: (0, indices.len()),
            vertices: (0, vertices.len()),

            albedo: None,
            metallic_roughness: None,
            occlusion: None,
            normal: None,
            emissive: None,
        };

        let mut shapes = Vec::new();
        shapes.push(shape);

        if let Some(unitization) = unitization {
            unitize_model(
                indices,
                vertices,
                primitive_parameters,
                &shapes,
                unitization.box_min,
                unitization.box_max,
                unitization.unitize_if_fits,
            );
        }

        shapes[0].indices.0 = self.indices.len();
        shapes[0].vertices.0 = self.vertices.len();

        self.indices.extend(indices.iter());
        self.vertices.extend(vertices.iter());
        self.primitive_parameters.extend(primitive_parameters.iter());
        self.shapes.append(&mut shapes);

        self.generate_indirect_buffer();
        self.update_buffers().chain_err(|| "Could not add shape")
    }

    pub fn generate_indirect_buffer(&mut self) {
        let mut draws = Vec::new();
        for shape in &self.shapes {
            draws.push(DrawElementsIndirectCommand {
                first_index: shape.indices.0 as u32,
                count: shape.indices.1 as u32,
                base_vertex: shape.vertices.0 as u32,

                instance_count: 1 as u32,
                base_instance: 0 as u32,
            });
        }

        self.indirect_buffer =
            Some(MutableBuffer::new(draws.as_slice()).expect("Could not create new indirect buffer"));
    }

    /// Gets all texture references excluding ibl textures.
    pub fn get_texture_references(&self) -> Vec<&Texture2D> {
        self.textures.iter().map(|t| (*t).borrow()).collect()
    }

    /// QoL function used to index into texture array.
    pub fn texture<I: Into<Option<usize>>>(&self, i: I) -> Option<&Texture2D> {
        let index = i.into();
        if index.is_none() {
            None
        } else {
            Some(&self.textures[index.unwrap()])
        }
    }
}

pub fn unitize_model(
    indices: &[u32],
    vertices: &[Vertex],
    primitive_parameters: &mut [PrimitiveParameters],
    shapes: &[Shape],
    box_min: Point3<f32>,
    box_max: Point3<f32>,
    unitize_if_fits: bool,
) {
    let mut min_pos = [f32::INFINITY; 3];
    let mut max_pos = [f32::NEG_INFINITY; 3];

    for (i, shape) in shapes.iter().enumerate() {
        for j in shape.indices.0..shape.indices.0 + shape.indices.1 {
            let index = indices[j] as usize + shape.vertices.0;
            let model_matrix = primitive_parameters[i].model_matrix;
            let raw_pos = Vector4::new(
                vertices[index].position[0],
                vertices[index].position[1],
                vertices[index].position[2],
                1.0,
            );
            let current_pos = model_matrix * raw_pos;
            for k in 0..3 {
                min_pos[k] = f32::min(min_pos[k], current_pos[k]);
                max_pos[k] = f32::max(max_pos[k], current_pos[k]);
            }
        }
    }

    let mut fits = true;
    for i in 0..3 {
        if min_pos[i] < box_min[i] || max_pos[i] > box_max[i] {
            fits = false;
        }
    }

    if fits && unitize_if_fits {
        return;
    }

    let max_box_side_length = (box_max[0] - box_min[0]).max((box_max[1] - box_min[1]).max(box_max[2] - box_min[2]));
    let max_model_side_length = (max_pos[0] - min_pos[0]).max((max_pos[1] - min_pos[1]).max(max_pos[2] - min_pos[2]));

    let scale = max_box_side_length / max_model_side_length;
    let matrix =
        // translate so center of model is center of box
        Matrix4::from_translation(
            Vector3::new(
                (max_pos[0] - min_pos[0]) * scale / -2.0 + (box_min[0] + box_max[0]) / 2.0,
                (max_pos[1] - min_pos[1]) * scale / -2.0 + (box_min[0] + box_max[0]) / 2.0,
                (max_pos[2] - min_pos[2]) * scale / -2.0 + (box_min[0] + box_max[0]) / 2.0
            )
          )
        // scale
        * Matrix4::from_scale(scale)
        // translate to (0, 0, 0)
        * Matrix4::from_translation(Vector3::new(-min_pos[0], -min_pos[1], -min_pos[2]));

    for primitive_parameter in primitive_parameters {
        primitive_parameter.model_matrix = matrix * primitive_parameter.model_matrix;
    }
}

fn supported_file_format<T: AsRef<Path>>(filepath: T) -> Result<bool> {
    let supported_extensions = ["obj", "gltf", "mall"];
    for extension in supported_extensions.iter() {
        let filepath_extension = match filepath.as_ref().extension() {
            Some(ext) => match ext.to_str() {
                Some(e) => e,
                None => bail!("Could not convert extracted extension to string"),
            },
            None => bail!("Could not get extension from filepath"),
        };
        if &filepath_extension == extension {
            return Ok(true);
        }
    }

    Ok(false)
}
