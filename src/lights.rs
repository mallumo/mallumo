use errors::*;
use mallumo_gls::*;
use shape_list::*;

use cgmath::{perspective, Deg, Matrix4, Point3, Vector3, Vector4};

/// GPU structure for light.
#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightParameter {
    pub pos_far: Vector4<f32>,
    pub color: Vector3<f32>,
    pub has_shadowmap: u32,
}

impl LightParameter {
    /// Creates new Light Parameter.
    pub fn new(pos: Point3<f32>, far: f32, color: Vector3<f32>, has_shadowmap: bool) -> LightParameter {
        LightParameter {
            pos_far: Vector4::new(pos[0], pos[1], pos[2], far),
            color: color,
            has_shadowmap: has_shadowmap as u32,
        }
    }
}

/// Combination of Light Parameter and cubemap Texture - shadowmap.
pub struct Light {
    pub shadowmap: Option<TextureCubemap>,
    pub light_parameter: LightParameter,
}

impl Light {
    /// Creates new Light.
    pub fn new<T: Into<Option<usize>>>(
        shadowmap_size: T,
        pos: Point3<f32>,
        far: f32,
        color: Vector3<f32>,
    ) -> Result<Light> {
        match shadowmap_size.into() {
            Some(shadowmap_size) => {
                let shadowmap = TextureCubemap::new_empty(
                    TextureCubemapSize(shadowmap_size, shadowmap_size),
                    TextureInternalFormat::DepthComponent32F,
                    TextureFormat::DepthComponent,
                    TextureDataType::Float,
                    TextureCubemapParameters {
                        min: TextureTexelFilter::Nearest,
                        mag: TextureTexelFilter::Nearest,
                        wrap_s: TextureWrapMode::ClampToEdge,
                        wrap_t: TextureWrapMode::ClampToEdge,
                        wrap_r: TextureWrapMode::ClampToEdge,
                        seamless: TextureSeamless::True,
                        ..Default::default()
                    },
                    1,
                )
                .chain_err(|| "Could not create shadows cubemap texture")?;

                Ok(Light {
                    shadowmap: Some(shadowmap),
                    light_parameter: LightParameter::new(pos, far, color, true),
                })
            }
            None => Ok(Light {
                shadowmap: None,
                light_parameter: LightParameter::new(pos, far, color, false),
            }),
        }
    }
}

/// Multiple Lights, their Light Parameters are stored in one buffer.
pub struct LightGroup {
    pub lights: Vec<Light>,
    pub light_parameters_buffer: MutableBuffer,

    pipeline: Box<Pipeline>,

    framebuffer: GeneralFramebuffer,

    globals_buffer: MutableBuffer,
}

impl<'a> LightGroup {
    /// Creates new Light Group from Lights.
    ///
    /// Must take ownership of Lights because shadowmaps are transfered to Light Group.
    pub fn new(lights: Vec<Light>) -> Result<LightGroup> {
        let light_parameters = lights
            .iter()
            .map(|light| light.light_parameter)
            .collect::<Vec<LightParameter>>();
        let light_parameters_buffer =
            MutableBuffer::new(&light_parameters).chain_err(|| "Unable to create light parameters buffer")?;

        let vertex = Shader::new(
            ShaderType::Vertex,
            &[include_str!("../assets/shaders/shadows_vertex.glsl")],
        )
        .chain_err(|| "Failed to compile shadows vertex shader")?;
        let geometry = Shader::new(
            ShaderType::Geometry,
            &[include_str!("../assets/shaders/shadows_geometry.glsl")],
        )
        .chain_err(|| "Failed to compile shadows geometry shader")?;
        let fragment = Shader::new(
            ShaderType::Fragment,
            &[include_str!("../assets/shaders/shadows_fragment.glsl")],
        )
        .chain_err(|| "Failed to compile shadows fragment shader")?;

        let pipeline = PipelineBuilder::new()
            .vertex_shader(&vertex)
            .geometry_shader(&geometry)
            .fragment_shader(&fragment)
            .build()
            .chain_err(|| "Unable to build shadows pipeline")?;

        let mut framebuffer = GeneralFramebuffer::new();
        framebuffer.set_enable(EnableOption::DepthTest);
        framebuffer.set_depth_test(DepthTest::LessOrEqual);
        framebuffer.set_enable(EnableOption::CullFace);
        framebuffer.set_cull_face(Face::Back);

        let globals_buffer = MutableBuffer::new_empty(400).chain_err(|| "Unable to create globals buffer")?;

        let light_group = LightGroup {
            lights: lights,
            light_parameters_buffer: light_parameters_buffer,

            pipeline: Box::new(pipeline),

            framebuffer: framebuffer,
            globals_buffer: globals_buffer,
        };

        Ok(light_group)
    }

    /// Renders shadowmaps from Shape Lists.
    pub fn render_shadowmaps(&mut self, renderer: &mut Renderer, shape_lists: &[&ShapeList]) -> Result<()> {
        for light in &mut self.lights {
            if let Some(ref mut shadowmap) = light.shadowmap {
                self.framebuffer.set_viewport(Viewport {
                    x: 0,
                    y: 0,
                    width: shadowmap.size.0,
                    height: shadowmap.size.1,
                });

                let aspect = shadowmap.size.0 as f32 / shadowmap.size.1 as f32;

                set_shadows_globals(&mut self.globals_buffer, light.light_parameter, aspect, 0.001)
                    .chain_err(|| "could not set shadowmap globals")?;

                let shadows_attachment = DrawTextureTarget {
                    depth_stencil: DepthStencilOption::Separate {
                        depth: DrawTextureAttachOption::AttachTexture(shadowmap),
                        stencil: DrawTextureAttachOption::None,
                    },
                    ..Default::default()
                };

                self.framebuffer
                    .attach_textures(&shadows_attachment)
                    .chain_err(|| "Could not attach shadowmap texture")?;
                renderer.clear_framebuffer(&self.framebuffer, ClearBuffers::Depth);

                for shape_list in shape_lists {
                    for (i, shape) in shape_list.shapes.iter().enumerate() {
                        let indices = shape.indices;
                        let vertices = shape.vertices;

                        let primitive_parameters_buffer = &shape_list.primitive_parameters_buffer;

                        let draw_command = DrawCommand::arrays(self.pipeline.as_ref(), 0, shape.indices.1)
                            .framebuffer(&self.framebuffer)
                            .attachments(&shadows_attachment)
                            .uniform(&self.globals_buffer, 0)
                            .storage_range_read::<u32>(&shape_list.indices_buffer, 0, indices.0, indices.1)
                            .storage_range_read::<Vertex>(&shape_list.vertices_buffer, 1, vertices.0, vertices.1)
                            .storage_range_read::<PrimitiveParameters>(primitive_parameters_buffer, 2, i, 1);

                        renderer.draw(&draw_command).chain_err(|| "Could not draw shadowmap")?;
                    }
                }
            }
        }

        Ok(())
    }

    pub fn get_buffer(&self) -> &MutableBuffer {
        &self.light_parameters_buffer
    }

    pub fn get_shadowmaps(&'a self) -> Vec<Option<&'a TextureCubemap>> {
        self.lights
            .iter()
            .map(|light| match light.shadowmap {
                Some(ref shadowmap) => Some(shadowmap),
                None => None,
            })
            .collect()
    }

    /// Updates Light Parameter buffer.
    ///
    /// Must be called after changing light_parameters(if changes are supposed to be seen on GPU side).
    pub fn update_buffer(&mut self) -> Result<()> {
        let light_parameters = self
            .lights
            .iter()
            .map(|light| light.light_parameter)
            .collect::<Vec<LightParameter>>();
        self.light_parameters_buffer
            .set_sub_data(&light_parameters, 0)
            .chain_err(|| "Could not update buffer")
    }
}

fn set_shadows_globals(buffer: &mut MutableBuffer, light: LightParameter, aspect: f32, near: f32) -> Result<()> {
    let light_pos: Point3<f32> = Point3::new(light.pos_far[0], light.pos_far[1], light.pos_far[2]);
    let shadowmap_perspective = perspective(Deg(90.0), aspect, near, light.pos_far[3]);
    buffer
        .set_sub_data(
            (shadowmap_perspective
                * Matrix4::look_at(
                    light_pos,
                    light_pos + Vector3::new(1.0, 0.0, 0.0),
                    Vector3::new(0.0, -1.0, 0.0),
                ))
            .as_ref() as &[f32; 16],
            0,
        )
        .chain_err(|| "Could not set matrix 1")?;
    buffer
        .set_sub_data(
            (shadowmap_perspective
                * Matrix4::look_at(
                    light_pos,
                    light_pos + Vector3::new(-1.0, 0.0, 0.0),
                    Vector3::new(0.0, -1.0, 0.0),
                ))
            .as_ref() as &[f32; 16],
            64,
        )
        .chain_err(|| "Could not set matrix 2")?;
    buffer
        .set_sub_data(
            (shadowmap_perspective
                * Matrix4::look_at(
                    light_pos,
                    light_pos + Vector3::new(0.0, 1.0, 0.0),
                    Vector3::new(0.0, 0.0, 1.0),
                ))
            .as_ref() as &[f32; 16],
            128,
        )
        .chain_err(|| "Could not set matrix 3")?;
    buffer
        .set_sub_data(
            (shadowmap_perspective
                * Matrix4::look_at(
                    light_pos,
                    light_pos + Vector3::new(0.0, -1.0, 0.0),
                    Vector3::new(0.0, 0.0, -1.0),
                ))
            .as_ref() as &[f32; 16],
            192,
        )
        .chain_err(|| "Could not set matrix 4")?;
    buffer
        .set_sub_data(
            (shadowmap_perspective
                * Matrix4::look_at(
                    light_pos,
                    light_pos + Vector3::new(0.0, 0.0, 1.0),
                    Vector3::new(0.0, -1.0, 0.0),
                ))
            .as_ref() as &[f32; 16],
            256,
        )
        .chain_err(|| "Could not set matrix 5")?;
    buffer
        .set_sub_data(
            (shadowmap_perspective
                * Matrix4::look_at(
                    light_pos,
                    light_pos + Vector3::new(0.0, 0.0, -1.0),
                    Vector3::new(0.0, -1.0, 0.0),
                ))
            .as_ref() as &[f32; 16],
            320,
        )
        .chain_err(|| "Could not set matrix 6")?;
    buffer
        .set_sub_data(light.pos_far.as_ref() as &[f32; 4], 384)
        .chain_err(|| "Could not set position and far plane")?;

    Ok(())
}
