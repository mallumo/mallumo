use super::*;

use glutin::{Api, ContextBuilder, EventsLoop, GlContext, GlProfile, GlRequest, GlWindow, WindowBuilder};

/// Holds window and OpenGL context.
///
/// You almost always want to use this as this abstracts the boilerplate needed
/// to even start using OpenGL while also providing basic event handling functionality.
pub struct App {
    pub title: String,
    pub width: usize,
    pub height: usize,

    events_loop: EventsLoop,
    pub gl_window: GlWindow,
    pub renderer: Renderer,
}

impl App {
    /// Creates new App with OpenGL 4.5 core profile.
    pub fn new(title: String, width: usize, height: usize, multisampling: u16, fullscreen: bool) -> App {
        let events_loop = EventsLoop::new();
        let mut window = WindowBuilder::new()
            .with_title(title.clone())
            .with_dimensions(width as u32, height as u32);
        if fullscreen {
            window = window.with_fullscreen(events_loop.get_available_monitors().nth(0));
        }
        let context = ContextBuilder::new()
            .with_gl(GlRequest::Specific(Api::OpenGl, (4, 5)))
            .with_gl_profile(GlProfile::Core)
            .with_depth_buffer(24)
            .with_multisampling(multisampling);
        let gl_window = GlWindow::new(window, context, &events_loop).expect("Could not create OpenGL window");

        let renderer = Renderer::new(gl_window.context()).expect("Unable to create renderer");
        unsafe {
            gl_window
                .context()
                .make_current()
                .expect("Unable to make context current");
        }

        App {
            title: title.clone(),
            width: width,
            height: height,

            events_loop: events_loop,
            gl_window: gl_window,
            renderer: renderer,
        }
    }

    /// Polls events.
    pub fn poll_events(&mut self) -> Vec<glutin::Event> {
        let mut events = Vec::new();

        self.events_loop.poll_events(|event| {
            events.push(event.clone());
        });

        events
    }

    /// Polls window events.
    pub fn poll_window_events(&mut self) -> Vec<glutin::WindowEvent> {
        let mut events = Vec::new();

        self.events_loop.poll_events(|event| match event {
            glutin::Event::WindowEvent { event, .. } => events.push(event.clone()),
            _ => {}
        });

        events
    }

    /// Swaps buffers.
    pub fn swap_buffers(&mut self) {
        self.gl_window.context().swap_buffers().expect("Could not swap buffers");
    }
}

/// Builder for App.
pub struct AppBuilder {
    title: String,
    width: usize,
    height: usize,
    multisampling: u16,
    fullscreen: bool,
}

impl AppBuilder {
    /// Creates new AppBuilder.
    pub fn new() -> AppBuilder {
        AppBuilder {
            title: "Window title".to_string(),
            width: 1280,
            height: 720,
            multisampling: 0,
            fullscreen: false,
        }
    }

    /// Changes window title.
    pub fn with_title(&mut self, title: &str) -> &mut AppBuilder {
        self.title = title.to_string();
        self
    }

    /// Changes window dimensions.
    pub fn with_dimensions(&mut self, width: usize, height: usize) -> &mut AppBuilder {
        self.width = width;
        self.height = height;
        self
    }

    /// Changes multisampling.
    pub fn with_multisampling(&mut self, multisampling: u16) -> &mut AppBuilder {
        self.multisampling = multisampling;
        self
    }

    pub fn with_fullscreen(&mut self, fullscreen: bool) -> &mut AppBuilder {
        self.fullscreen = fullscreen;
        self
    }

    /// Builds App based on AppBuilder parameters.
    pub fn build(&self) -> App {
        App::new(
            self.title.clone(),
            self.width,
            self.height,
            self.multisampling,
            self.fullscreen,
        )
    }
}
